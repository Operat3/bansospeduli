from django.urls import path
from .views import beranda, covid_api, login_beranda, logout_account

app_name = 'beranda'

urlpatterns = [
    path('', beranda, name='beranda'),
    path('login', login_beranda, name = 'login'),
    path('covid_api', covid_api, name = "covid_api"),
    path('logout/', logout_account, name='logout_beranda')
]