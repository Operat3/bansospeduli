$("#previous-toggler").click( () => {
    if ($("#previous-toggler").hasClass("rotate-180")) {
        $("#previous-toggler").removeClass("rotate-180");
        $(".hideable").slideToggle("slow");

    } else {
        $("#previous-toggler").addClass("rotate-180");
        $(".hideable").slideToggle("slow");
    }
}
);

$("#receiver-toggler").click( () => {
if ($("#receiver-toggler").hasClass("rotate-180")) {
    $("#receiver-toggler").removeClass("rotate-180");
    $(".receiver-info-container").slideToggle("slow");

} else {
    $("#receiver-toggler").addClass("rotate-180");
    $(".receiver-info-container").slideToggle("slow");
}
});

$("#package-info-toggler").click( () => {
if ($("#package-info-toggler").hasClass("rotate-180")) {
    $("#package-info-toggler").removeClass("rotate-180");
    $(".package-container").slideToggle("slow");

} else {
    $("#package-info-toggler").addClass("rotate-180");
    $(".package-container").slideToggle("slow");
}
});

$("#location-toggler").click( () => {
if ($("#location-toggler").hasClass("rotate-180")) {
    $("#location-toggler").removeClass("rotate-180");
    $(".area-container").slideToggle("slow");

} else {
    $("#location-toggler").addClass("rotate-180");
    $(".area-container").slideToggle("slow");
}
});

var active_count = 3;

function toggleQueued() {
if($("#queued-dash-btn").hasClass('btn-curious-selected') && active_count > 1) {
    $("#queued-dash-btn").removeClass('btn-curious-selected');
    $("#queued-bansos-section").fadeOut("fast");
    active_count--;
} else if (!$("#queued-dash-btn").hasClass('btn-curious-selected')) {
    $("#queued-dash-btn").addClass('btn-curious-selected');
    $("#queued-bansos-section").fadeIn("fast");
    active_count++;
}
}

function toggleSent() {
if ($("#sent-dash-btn").hasClass('btn-curious-selected') && active_count > 1) {
    $("#sent-dash-btn").removeClass('btn-curious-selected');
    $("#sent-bansos-section").fadeOut("fast");
    active_count--;
} else if(!$("#sent-dash-btn").hasClass('btn-curious-selected')){
    $("#sent-dash-btn").addClass('btn-curious-selected');
    $("#sent-bansos-section").fadeIn("fast");
    active_count++;
}
}

function toggleReceived() {
if ($("#received-dash-btn").hasClass('btn-curious-selected') && active_count > 1) {
    $("#received-dash-btn").removeClass('btn-curious-selected');
    $("#received-bansos-section").fadeOut("fast");
    active_count--;

} else if(!$("#received-dash-btn").hasClass('btn-curious-selected')) {
    $("#received-dash-btn").addClass('btn-curious-selected');
    $("#received-bansos-section").fadeIn("fast");
    active_count++;
}
}