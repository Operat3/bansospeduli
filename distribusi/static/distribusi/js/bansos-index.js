$(document).ready(bansos_update);


const grid_queued = $("#queued-bansos-grid");
const grid_sent = $("#sent-bansos-grid");
const grid_received = $("#received-bansos-grid");

var queued_count;
var sent_count;
var received_count;

function bansos_update() {
    // Get bansos-group information
    $.ajax({
        type: 'GET',
        url: '/distribusi/get-bansos-groups/',

        success: function(data) {

            // Initialize all with 0
            grid_queued.text("");
            grid_sent.text("");
            grid_received.text("");

            queued_count = 0;
            sent_count = 0;
            received_count = 0;

            for(let i = 0; i < data.length; i++) {
                var chosen_destination = null;
                var header_color = null;
                var corresponding_text;

                var bansos = data[i].fields;

                if(bansos.status == "queued") {
                    chosen_destination = grid_queued;
                    header_color = "red";
                    corresponding_text = "will be sent to ";
                    queued_count++;

                } else if(bansos.status == "sent") {
                    chosen_destination = grid_sent;
                    header_color = "orange";
                    corresponding_text = "is being sent to ";
                    sent_count++;

                } else {
                    chosen_destination = grid_received;
                    header_color = "green";
                    corresponding_text = "has been sent to ";
                    received_count++;
                }
            
                // Create card
                var card_div = document.createElement('div');
                card_div.className = "card card-one"
                
                // Card header red
                var card_header = document.createElement('div');
                card_header.className = "card-header " + header_color;

                var card_title = document.createElement('h5');
                card_title.className = "card-title";

                card_title.innerText = "Bansos to " + bansos.provinsi;

                card_header.appendChild(card_title);

                // Card body
                var card_body = document.createElement('div');
                card_body.className = "card-body";

                // Create subtitle
                var card_subtitle = document.createElement('h6');
                card_subtitle.className = "card-subtitle mb-2 text-muted";
                card_subtitle.innerText = data[i].pk;

                // Create p tag
                var card_text = document.createElement('p');
                card_text.className = "card-text";
                card_text.innerText = "Destination of this bansos is " + bansos.total_receiver + " citizens in " + bansos.provinsi + `. 
                This bansos ` + corresponding_text + bansos.destination_kelurahan + ", " + bansos.destination_kecamatan + ", " + bansos.destination_kabupaten_kota + ", " + bansos.provinsi + ". ";

                // Create card-footer
                var card_footer = document.createElement('div');
                card_footer.className = "card-one-footer";

                // a tag inside card-footer
                var detail_button = document.createElement('a');
                detail_button.className = "btn btn-primary";
                detail_button.setAttribute('href', "/distribusi/detail-bansos/" + data[i].pk);
                detail_button.innerText = "Bansos Details";

                card_footer.appendChild(detail_button);

                card_body.appendChild(card_subtitle);
                card_body.appendChild(card_text);
                card_body.appendChild(card_footer);

                card_div.appendChild(card_header);
                card_div.appendChild(card_body);

                chosen_destination.append(card_div);

                
            }
            $("#queued-title").text("Queued Bansos Group (" + queued_count + ")");
            $("#sent-title").text("Sent Bansos Group (" + sent_count +")");
            $("#received-title").text("Received Bansos Group (" + received_count + ")");
            
            setTimeout(bansos_update, 5000);
        },
    })
}