from django.urls import path
from .views import add_previous_location, all_bansos, bansos_from_id, bansos_group_detail, generate_bansos, generate_bansos_groups, generate_json_bansos_location, get_area_json, get_tracking_resp, mob_generate_bansos, mob_get_tracking_admin, mobile_get_bansos_groups, mobile_register_area, register_area, tracking_admin_dashboard, update_shipping

app_name = 'distribusi'

urlpatterns = [
    path('', all_bansos, name='all_bansos'),
    path('generate-bansos/', generate_bansos, name='generate_bansos'),
    path('update-shipping/<uuid:bansos_group_id>/', update_shipping, name='update_shipping'),
    path('detail-bansos/<uuid:bansos_group_id>/', bansos_group_detail, name='bansos_group_detail'),
    path('tracking-admin-dashboard/', tracking_admin_dashboard, name='tracking_admin_dashboard'),
    path('get-previous-location/', generate_json_bansos_location, name="generate_json_bansos_location"),
    path('get-bansos-groups/', generate_bansos_groups, name="generate_bansos_groups"),
    path('register-area/', register_area, name='register_area'),
    path('get-area-information/', get_area_json, name='get_area_json'),
    path('mob-get-bansos-groups/', mobile_get_bansos_groups, name="mobile_get_bansos_groups"),
    path('mob-bansos-from-id/', bansos_from_id, name="bansos_from_id"),
    path('mob-register-area/', mobile_register_area),
    path('mob-register-bansos/', mob_generate_bansos),
    path('mob-get-admin/', mob_get_tracking_admin),
    path('mob-get-trackings/', get_tracking_resp),
    path('mob-add-prev-location/', add_previous_location),
]