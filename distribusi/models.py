from django.db import models
import uuid

STATUS_CHOICE = [
    ('queued', 'queued'),
    ('sent', 'sent'),
    ('received', 'received'),
]

class Area(models.Model):
    area_code = models.CharField(max_length=20, primary_key=True)

    area_name = models.CharField(max_length=20)

class BansosGroup(models.Model):
    responsible_admin = models.ForeignKey('staff.Admin', on_delete=models.RESTRICT, default='')
    id_bansos_group = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # Kalau areanya hilang, harusnya Bansosnya juga hilang :)

    # Receiving provinsi marks the target provinsi
    receiving_provinsi = models.ForeignKey(Area, 
        on_delete=models.SET_DEFAULT, default='NULL')

    status = models.CharField(max_length=10,  
            default='queued')

    kelurahan = models.CharField(max_length=20)
    kecamatan = models.CharField(max_length=20)
    kabupaten_kota = models.CharField(max_length=20, null=True)
    # The variable provinsi marks the CURRENT provinsi
    provinsi = models.CharField(max_length=20)

    destination_kelurahan = models.CharField(max_length=20, null=True)
    destination_kecamatan = models.CharField(max_length=20, null=True)
    destination_kabupaten_kota = models.CharField(max_length=20, null=True)

    total_receiver = models.BigIntegerField(default=0)
    # JSON to store dictionary (multivalued), 
    # Key --> Jenis
    # Value --> Jumlah
    bentuk_bantuan = models.JSONField(default=dict)
    satuan_bantuan = models.JSONField(default=dict)

   

class Bansos(models.Model):
    owning_group = models.ForeignKey(BansosGroup,
        on_delete=models.CASCADE,
    )

    id_bansos = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    received_by = models.ForeignKey('user.AccountMasyarakat', on_delete=models.CASCADE, default=None)
    is_reported = models.BooleanField(default=False)

class BansosPreviousLocation(models.Model):
    bansos_group = models.ForeignKey(BansosGroup, on_delete=models.CASCADE)
    kelurahan = models.CharField(max_length=20)
    kecamatan = models.CharField(max_length=20)
    kabupaten_kota = models.CharField(max_length=20, null=True)
    provinsi = models.CharField(max_length=20)
    status = models.CharField(max_length=20, default='queued')
    time_stamp = models.DateTimeField(auto_now=True)
