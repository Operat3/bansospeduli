from importlib import import_module
from json.decoder import JSONDecoder
from django.conf import settings
from django.http.response import HttpResponse, HttpResponseForbidden, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators import csrf
from user.models import Account
from staff.models import Admin
from .models import Area, Bansos, BansosGroup, BansosPreviousLocation, STATUS_CHOICE
from laporan.forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.db import IntegrityError
from django.contrib.sessions.backends.db import SessionStore
import json
import time

# UPDATED
@login_required()
def tracking_admin_dashboard(request):
    # Add additional variable if admin is a tracking admin and is currently occupied
    user = request.user

    try:
        admin = user.admin
    
    except Admin.DoesNotExist:
        return HttpResponseForbidden()

    if admin.type != 'tracker':
        return HttpResponseForbidden()

    bansos_all = admin.bansosgroup_set.all()
    context = {'tracking_admin':admin, 'bansos_all':bansos_all}
    
    return render(request, 'tracking-admin-dashboard.html', context)

# UPDATED
@login_required()
def all_bansos(request):
    '''
    Function as index for distribusi application,
    it displays all the bansos generated (on queue, sent,received)
    '''
    user = request.user

    try:
        admin = user.admin
    
    except Admin.DoesNotExist:
        return HttpResponseForbidden()

    if admin.type != 'generator':
        return HttpResponseForbidden()

    context = {'admin':admin}

    return render(request, 'bansos-index.html', context)

def check_number(string_number: str) -> bool:
    try:
        float(string_number)
        return float(string_number) > 0

    except ValueError:
        return False


# UPDATED
@login_required()
def generate_bansos(request):
    '''
    Function to generate bansos. It will save the bansos generated
    for a specific area and assign it to citizens of that area.
    It will also assign a tracking admin to the bansos generated.
    '''
    user = request.user

    post_request_errors = []

    # Authentication

    # Reject if user is not an admin
    try:
        user_admin_account = Admin.objects.get(owning_user=user)

    except Admin.DoesNotExist:
        return HttpResponseForbidden()
    
    if user_admin_account.type != 'generator':
        return HttpResponseForbidden()

    
    if request.method == 'POST':
        # Input validation
        # Sent-from information
        current_kelurahan = request.POST.get('input_kelurahan')
        current_kecamatan = request.POST.get('input_kecamatan')
        current_kabupaten_kota = request.POST.get('input_kabupaten_kota')
        current_provinsi = request.POST.get('input_provinsi')

        # Sent-to information
        target_kelurahan = request.POST.get('input_kelurahan_target')
        target_kecamatan = request.POST.get('input_kecamatan_target')
        target_kabupaten_kota = request.POST.get('input_kabupaten_kota_target')

        area_code = request.POST.get('area')

        tracking_admin_tele = request.POST.get('tracking-admin')
        
        error_flag = False

        # Check if any of the fields are empty
        if not current_kelurahan:
            post_request_errors.append("Kelurahan Field Must Not Be Empty!")
            error_flag = True

        if not current_kecamatan:
            post_request_errors.append("Kecamatan Field Must Not Be Empty!")
            error_flag = True

        if not current_provinsi:
            post_request_errors.append("Provinsi Field Must Not Be Empty!")
            error_flag = True

        if not area_code:
            post_request_errors.append("Area Field Must Not Be Empty!")
            error_flag = True

        if not tracking_admin_tele:
            post_request_errors.append("Tracking Admin Field Must Not Be Empty!")
            error_flag = True
        
        if not error_flag:
            # Check if bansos type is empty
            # Create a dictionary (to be converted to JSON) from the given bansos-type and values
            counter_value = int(request.POST.get('item-counter'))
            jenis_bantuan = {}
            satuan_bantuan = {}

            bansos_error_flag = False

            # Iterate through the user-added fields, check if it contains valid value
            for type_count in range(counter_value+1):
                bantuan_name = request.POST.get(f"type_input{type_count}")
                bantuan_value = request.POST.get(f"type_value{type_count}")
                bantuan_unit = request.POST.get(f"type_unit{type_count}")

                if bantuan_name and bantuan_value and bantuan_unit:
                    # Insert value to dictionary when validated
                    jenis_bantuan[bantuan_name.strip().lower()] = bantuan_value.strip().lower()
                    satuan_bantuan[bantuan_name.strip().lower()] = bantuan_unit.strip().lower()

                    if not check_number(bantuan_value):
                        bansos_error_flag = True
                
                # If only bantuan_name is given
                elif bantuan_name:
                    bansos_error_flag = True

                ## If either bantuan_value or bantuan_unit is not empty, but bantuan_name is empty,
                ## the entry will be neglected
            
            # Raise error if no jenis bantuan is given
            if not jenis_bantuan or not satuan_bantuan or bansos_error_flag:
                post_request_errors.append("Bansos information is invalid.")
                
            else:
                # Get area from the given area code
                area_object = Area.objects.get(area_code=area_code)

                # Get tracking admin from the given telephone number
                admin_object = Account.objects.get(telephone=tracking_admin_tele).admin

                # Get all rightful bansos receiver in the area
                citizens = area_object.accountmasyarakat_set.filter(is_verified=True)

                # Clean current provinsi data
                current_provinsi_name = current_provinsi.split(',')[1]

                # Reject generation of bansos if no rightful citizens in the area
                if not len(citizens) > 0:
                    post_request_errors.append("Area has no citizens.")

                else:
                    # Generate and Save BansosGroup
                    new_bansos_group = BansosGroup(
                        responsible_admin=admin_object,
                        receiving_provinsi=area_object,
                        status='queued',
                        kelurahan=current_kelurahan,
                        kecamatan=current_kecamatan,
                        kabupaten_kota=current_kabupaten_kota,
                        provinsi=current_provinsi_name,
                        destination_kelurahan=target_kelurahan,
                        destination_kecamatan=target_kecamatan,
                        destination_kabupaten_kota=target_kabupaten_kota,
                        total_receiver=len(citizens),
                        bentuk_bantuan=jenis_bantuan,
                        satuan_bantuan=satuan_bantuan,
                    )

                    # Generate previous location data
                    bansos_location = BansosPreviousLocation(
                        bansos_group=new_bansos_group,
                        kelurahan=current_kelurahan,
                        kecamatan=current_kecamatan,
                        kabupaten_kota=current_kabupaten_kota,
                        provinsi=current_provinsi_name
                    )

                    # Save to database
                    new_bansos_group.save()
                    bansos_location.save()

                    # Generate individual bansos for each rightful citizens
                    for citizen in citizens:
                        new_bansos = Bansos(
                            owning_group=new_bansos_group,
                            received_by=citizen
                        )

                        new_bansos.save()
                    
                    # As the tracking staff is selected to track the just-generated bansos,
                    # changed is_occupied flag to True
                    admin_object.is_occupied = True
                    admin_object.save()

                    return redirect('distribusi:all_bansos')
    
    
    # Retrieve the query set of all tracking admins, 
    # and obtain its corresponding account
    tracking_admins = Admin.objects.filter(type="tracker")

    tracking_accounts = [ tracking.owning_user for tracking in tracking_admins ]

    # Retrieve the query set of all areas for bansos destination
    areas = Area.objects.all()

    context = {'tracking_accounts': tracking_accounts, 'areas': areas, 'generating_account':user, 'errors':post_request_errors}

    return render(request, 'generate-bansos.html', context)

# UPDATED
@login_required()
def update_shipping(request, bansos_group_id):
    '''
    Function to update the current shipping position of the bansos
    group. It will take bansos_group_id in url parameter.
    '''
    post_request_errors = []

    user = request.user
    # Authorization Validation
    try:
        user_admin_account = Admin.objects.get(owning_user=user)

    except Admin.DoesNotExist:
        return HttpResponseForbidden()

    # Check if user is not of type tracking admin
    if user_admin_account.type != 'tracker':
        return HttpResponseForbidden()

    # Get corresponding bansos_group from given bansos_group_id
    corresponding_group = BansosGroup.objects.get(id_bansos_group=bansos_group_id)

    # Check if bansos does not belong to the current admin
    if corresponding_group.responsible_admin != user_admin_account:
        return HttpResponseForbidden()

    if request.method == 'POST':
        # Validate input
        new_kelurahan = request.POST.get('new-kelurahan')
        new_kecamatan = request.POST.get('new-kecamatan')
        new_kabupaten_kota = request.POST.get('new-kabupaten-kota')
        new_provinsi = request.POST.get('new-provinsi')
        status = request.POST.get('new-status')

        # Process new_provinsi
        new_provinsi_code = new_provinsi.split(',')[0]
        new_provinsi_name = new_provinsi.split(',')[1]
        error_flag = False

        # Check if any input is empty
        if not new_kelurahan:
            post_request_errors.append("Kelurahan Field Must Not Be Empty!")
            error_flag = True

        if not new_kecamatan:
            post_request_errors.append("Kecamatan Field Must Not Be Empty!")
            error_flag = True

        if not new_provinsi:
            post_request_errors.append("Provinsi Field Must Not Be Empty!")
            error_flag = True

        if not status:
            post_request_errors.append("Status Field Must Not Be Empty!")
            error_flag = True

        if (corresponding_group.status == 'received'):
            post_request_errors.append("Reupdating a received bansos is a Forbidden Operation.")
            error_flag = True

        if (status == 'received' and (new_kabupaten_kota != corresponding_group.destination_kabupaten_kota \
                or new_provinsi_code != corresponding_group.receiving_provinsi.area_code)):

            post_request_errors.append("New inserted location is not receiving area.")
            error_flag = True

        if not error_flag:
            # Save information if they are valid
            corresponding_group.kelurahan = new_kelurahan
            corresponding_group.kecamatan = new_kecamatan
            corresponding_group.kabupaten_kota = new_kabupaten_kota
            corresponding_group.provinsi = new_provinsi_name
            corresponding_group.status = status
            corresponding_group.save()

            # Instantiate new location to past location model
            new_location = BansosPreviousLocation(
                bansos_group=corresponding_group,
                kelurahan=new_kelurahan,
                kecamatan=new_kecamatan,
                kabupaten_kota=new_kabupaten_kota,
                provinsi=new_provinsi_name,
                status=status,
            )

            new_location.save()

            # Change status to unoccupied if bansos has been received by Area
            if status == 'received':
                responsible_admin = corresponding_group.responsible_admin
                responsible_admin.is_occupied = False
                responsible_admin.save()


            # Redirect to Dashboard
            return redirect('distribusi:tracking_admin_dashboard')

    
    # Get group's past location
    past_locations = corresponding_group.bansospreviouslocation_set.all()

    context = {
        'corresponding_group':corresponding_group, 
        'past_locations':past_locations,
        'status_choice':STATUS_CHOICE,
        'area_options': Area.objects.all(),
        'errors':post_request_errors,
    }

    return render(request, 'update-shipping.html', context)


@login_required()
def bansos_group_detail(request, bansos_group_id):
    '''
    Function to display all information about a specific
    bansos group, based on its ID. This request only use
    get method
    '''

    user = request.user

    try:
        user_account_admin = Admin.objects.get(owning_user=user)
    
    except Admin.DoesNotExist:
        return HttpResponseForbidden()

    # Restrict to only Generating Admin
    if user_account_admin.type != 'generator':
        return HttpResponseForbidden()

        
    # Get the corresponding group from the given id
    bansos_group = BansosGroup.objects.get(id_bansos_group=bansos_group_id)

    # Retrieve query set of all past location
    past_locations = bansos_group.bansospreviouslocation_set.all()

    # Retrieve query set of all the bansos receiver (everyone in the area)
    receiver = bansos_group.receiving_provinsi.accountmasyarakat_set.all()

    # Get number of receiver
    number_of_packages = len(bansos_group.bansos_set.all())
    
    # Convert to integer
    type_converted = [ (jenis, float(value)*number_of_packages, bansos_group.satuan_bantuan[jenis]) for jenis, value in bansos_group.bentuk_bantuan.items() ]
    package_with_unit = [ (jenis, float(value), bansos_group.satuan_bantuan[jenis]) for jenis, value in bansos_group.bentuk_bantuan.items() ]
    context = {
        'bansos_group':bansos_group,
        'past_locations':past_locations,
        'receiver':receiver,
        'number_of_packages':number_of_packages,
        'package_with_unit':package_with_unit,
        'type_converted':type_converted
    }
    
    return render(request, 'bansos-detail.html', context)

def generate_json_bansos_location(request):
    # Get all previous locations
    previous_locations = BansosPreviousLocation.objects.all()
    # Serialize query set to json
    data = serializers.serialize('json', previous_locations)

    return HttpResponse(data, content_type='application/json')


def generate_bansos_groups(request):
    # Get all bansos group
    all_bansos_groups = BansosGroup.objects.all()
    # Serialize query set to json
    data = serializers.serialize('json', all_bansos_groups)

    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def mobile_get_bansos_groups(request):
    # Get all bansos group
    all_bansos_groups = BansosGroup.objects.all()

    all_bansos_list = []
    for group in all_bansos_groups:
        group_dict = {
            "pk":str(group.id_bansos_group),
            "fields": {
                "responsible_admin": group.responsible_admin.owning_user.telephone,
                "receiving_provinsi":group.receiving_provinsi.area_name,
                "status":group.status,
                "destination_kelurahan":group.destination_kelurahan,
                "destination_kecamatan":group.destination_kecamatan,
                "destination_kabupaten_kota":group.destination_kabupaten_kota,
                "total_receiver":group.total_receiver,
            }
        }
        all_bansos_list.append(group_dict)
    

    # Serialize query set to json
    data = json.dumps(all_bansos_list)

    return HttpResponse(data, content_type='application/json')

def bansos_from_id(request):
    bansos_id = request.GET.get("bansos-id")
    corresponding_group = BansosGroup.objects.filter(id_bansos_group=bansos_id)
    
    if corresponding_group:
        corresponding_group = corresponding_group[0]

        corresponding_data = {
            "pk":str(corresponding_group.id_bansos_group),
            "admin": {
                "name":corresponding_group.responsible_admin.name,
                "telephone":corresponding_group.responsible_admin.owning_user.telephone
            },
            "area":{
                "area_code": corresponding_group.receiving_provinsi.area_code,
                "des_urban_village":corresponding_group.destination_kelurahan,
                "des_subdistrict":corresponding_group.destination_kecamatan,
                "des_city":corresponding_group.destination_kabupaten_kota,
                "des_provinsi":corresponding_group.receiving_provinsi.area_name,
            },
            "bentuk_bantuan": corresponding_group.bentuk_bantuan,
            "satuan_bantuan": corresponding_group.satuan_bantuan,
            "total_receiver": corresponding_group.total_receiver,
        }

        previous_locations = BansosPreviousLocation.objects.filter(bansos_group=corresponding_group)

        location_dictionaries = []
        for prev in previous_locations:
            tempDict = {
                "kelurahan":prev.kelurahan,
                "kecamatan":prev.kecamatan,
                "kabupaten_kota":prev.kabupaten_kota,
                "provinsi":prev.provinsi,
                "status":prev.status,
                "time_stamp":str(prev.time_stamp),
            }

            location_dictionaries.append(tempDict)
        
        corresponding_data["previous_locations"] = location_dictionaries

        # Get total package informations


        # Get corresponding previous locations

        data = json.dumps(corresponding_data)

        return HttpResponse(data, content_type='application/json')
    else:
        return HttpResponse("Object not found")

@csrf_exempt
def mobile_register_area(request): 
    if request.method == 'POST':
        data = json.loads(request.body.decode("utf-8"))

        area_code = data.get('area-code')
        area_name = data.get('area-name')

        # Check if the area_code has been registered
        foundArea = Area.objects.filter(area_code=area_code.strip())

        if foundArea:
            successReport = {
                'isSuccessful':False
            }

        else:
            Area.objects.create(area_code=area_code.strip(), area_name=area_name.strip())
            successReport = {
                'isSuccessful':True
            }

        data = json.dumps(successReport)

        return HttpResponse(data, content_type='application/json')

    return HttpResponseForbidden("403 Forbidden Access")

@login_required
def register_area(request):
    '''
    Function to register a new area to database.
    '''
    user = request.user

    try:
        admin = user.admin
    except Admin.DoesNotExist:
        return HttpResponseForbidden()
    
    if admin.type != 'generator':
        return HttpResponseForbidden()
        
    # Container to store error messages
    message_flash = []

    if request.method == "POST":
        # Get corresponding data
        area_code = request.POST['provinsi-code']
        area_name = request.POST['provinsi-name']

        # Give error message when either field is empty
        if not area_code or not area_name:
            message_flash.append("[Rejected] Input was empty.")
        
        else:
            
            new_provinsi = Area(area_code=area_code, area_name=area_name)
            new_provinsi.save()
            message_flash.append("[Successful] Area with code " + area_code + " was registered successfully.")
                
            

    context = {'messages':message_flash}
    return render(request, 'add-area.html', context)

@csrf_exempt
def mob_generate_bansos(request):
    if request.method == 'POST':
        # Input validation
        # Sent-from information
        data = json.loads(request.body.decode("utf-8"))

        current_kelurahan = data.get('input_kelurahan')
        current_kecamatan = data.get('input_kecamatan')
        current_kabupaten_kota = data.get('input_kabupaten_kota')
        current_provinsi = data.get('input_provinsi')

        # Sent-to information
        target_kelurahan = data.get('input_kelurahan_target')
        target_kecamatan = data.get('input_kecamatan_target')
        target_kabupaten_kota = data.get('input_kabupaten_kota_target')

        area_code = data.get('area')

        tracking_admin_tele = data.get('tracking-admin')
        
        error_flag = False

    # Check if bansos type is empty
    # Create a dictionary (to be converted to JSON) from the given bansos-type and values
    bansosValue = data.get('bansosValue')
    
    jenis_bantuan = bansosValue

    for i in jenis_bantuan:
        jenis_bantuan[i] = jenis_bantuan[i]

    bansosUnit = data.get('bansosUnit')
    satuan_bantuan = bansosUnit

    # Get area from the given area code
    area_object = Area.objects.get(area_code=area_code)

    # Get tracking admin from the given telephone number
    admin_object = Account.objects.get(telephone=tracking_admin_tele).admin

    # Get all rightful bansos receiver in the area
    citizens = area_object.accountmasyarakat_set.filter(is_verified=True)

    # Clean current provinsi data
    current_provinsi_name = Area.objects.get(area_code=current_provinsi).area_name

    # Reject generation of bansos if no rightful citizens in the area
    if not len(citizens) > 0:
        return JsonResponse({'isSuccessful':False, 'error':'Area has no citizens.'}, safe=False)

    else:
        # Generate and Save BansosGroup
        new_bansos_group = BansosGroup(
            responsible_admin=admin_object,
            receiving_provinsi=area_object,
            status='queued',
            kelurahan=current_kelurahan,
            kecamatan=current_kecamatan,
            kabupaten_kota=current_kabupaten_kota,
            provinsi=current_provinsi_name,
            destination_kelurahan=target_kelurahan,
            destination_kecamatan=target_kecamatan,
            destination_kabupaten_kota=target_kabupaten_kota,
            total_receiver=len(citizens),
            bentuk_bantuan=jenis_bantuan,
            satuan_bantuan=satuan_bantuan,
        )

        # Generate previous location data
        bansos_location = BansosPreviousLocation(
            bansos_group=new_bansos_group,
            kelurahan=current_kelurahan,
            kecamatan=current_kecamatan,
            kabupaten_kota=current_kabupaten_kota,
            provinsi=current_provinsi_name
        )

        # Save to database
        new_bansos_group.save()
        bansos_location.save()

        # Generate individual bansos for each rightful citizens
        for citizen in citizens:
            new_bansos = Bansos(
                owning_group=new_bansos_group,
                received_by=citizen
            )

            new_bansos.save()
        
        # As the tracking staff is selected to track the just-generated bansos,
        # changed is_occupied flag to True
        admin_object.is_occupied = True
        admin_object.save()

    return JsonResponse({'isSuccessful':True}, safe=False)

def mob_get_tracking_admin(request):
    admins = Admin.objects.all()

    admin_list = []
    for admin in admins:
        if admin.type == 'tracker' and not admin.is_occupied:
            admin_acc = admin.owning_user
            tempDict = {
                'name':admin.name,
                'telephone':admin_acc.telephone,
            }
            admin_list.append(tempDict)

    data = json.dumps(admin_list)
    
    return HttpResponse(data, content_type='application/json')

def get_area_json(request):
    '''
    Method to return Area Objects to aid Ajax Implementation in generating bansos.
    '''
    area_active = {}
    areas = Area.objects.all()

    for area in areas:
        area_active[area.area_code] = {
        
            "area_code": area.area_code,
            "area_name": area.area_name,
            "number_of_receiver": len(area.accountmasyarakat_set.filter(is_verified=True)),
            "assigned_bansos": [ (str(bansos.id_bansos_group), bansos.status) for bansos in area.bansosgroup_set.all() ],
        }

    data = json.dumps(area_active)
    
    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def get_tracking_resp(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode("utf-8"))
        session_id = data.get('session_id')

        engine = import_module(settings.SESSION_ENGINE)
        sessionstore = engine.SessionStore
        session = sessionstore(session_id)
        
        tracking_phone_number = session.get("_auth_user_id")

        # Get corresponding admin
        tracking_admin = Account.objects.get(telephone=tracking_phone_number)

        try:
            admin_obj = tracking_admin.admin
            if admin_obj.type != 'tracker':
                return HttpResponseForbidden("Oh boi!")

            else:
                corr_bansos = BansosGroup.objects.filter(responsible_admin=admin_obj)

                tempList = []
                for bansos in corr_bansos:
                    tempDict = {
                        "pk": str(bansos.id_bansos_group),
                        "fields": {
                            "status":bansos.status,
                            "receiving_provinsi":bansos.receiving_provinsi.area_name,
                            "total_receiver":bansos.total_receiver,
                            "destination_kelurahan":bansos.destination_kelurahan,
                            "destination_kecamatan":bansos.destination_kecamatan,
                            "destination_kabupaten_kota":bansos.destination_kabupaten_kota,
                            "time_stamp":str(bansos.bansospreviouslocation_set.all()[0].time_stamp),
                        }
                    }

                    tempList.append(tempDict)

                data = json.dumps(tempList)

                return HttpResponse(data, content_type='application/json')

        except Exception as e:
            print(e)

        return HttpResponse(tracking_phone_number)

    return HttpResponse('Hello')

@csrf_exempt
def add_previous_location(request):
    if request.method == 'POST':

        data = json.loads(request.body.decode("utf-8"))

        bansos_id = data.get('group-id')
        kelurahan = data.get('kelurahan')
        kecamatan = data.get('kecamatan')
        kabupaten_kota = data.get('kabkota')
        provinsi = data.get('provinsi')
        status = data.get('status')

        if bansos_id and kelurahan and kecamatan and kabupaten_kota and provinsi and status:
            # Get bansos from id
            bansos_group = BansosGroup.objects.get(id_bansos_group=bansos_id)

            if (bansos_group.status == 'received'):
                return JsonResponse({'isSuccessful':False,"error":"Received bansos cannot be edited."}, safe=False)

            if (status == 'received' and (kabupaten_kota != bansos_group.destination_kabupaten_kota \
                or provinsi != bansos_group.receiving_provinsi.area_code)):

                return JsonResponse({'isSuccessful':False,"error":"New inserted location is not receiving area."}, safe=False)

            bansos_group.kelurahan = kelurahan
            bansos_group.kecamatan = kecamatan
            bansos_group.kabupaten_kota = kabupaten_kota
            bansos_group.provinsi = Area.objects.get(area_code=provinsi).area_name
            bansos_group.status = status
            bansos_group.save()

            BansosPreviousLocation.objects.create(
                bansos_group=bansos_group,
                kelurahan=kelurahan,
                kecamatan=kecamatan,
                kabupaten_kota=kabupaten_kota,
                provinsi=bansos_group.provinsi,
                status=status,
            )

            if status == 'received':
                responsible_admin = bansos_group.responsible_admin
                responsible_admin.is_occupied = False
                responsible_admin.save()

            return JsonResponse({'isSuccessful':True}, safe=False)
        
        return JsonResponse({'isSuccessful':False}, safe=False)


    return JsonResponse({'isSuccessful':False}, safe=False)

