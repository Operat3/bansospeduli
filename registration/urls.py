from django.contrib import admin
from django.urls import include, path
from .views import *

urlpatterns = [
    path('admin/', RegisterAdmin, name='RegisterAdmin'),
    path('citizen/', RegisterCitizen, name='RegisterCitizen'),
    path('api/', getAllPhoneNumber),
    path('flutter/get/all/phone/', get_all_phone_number),
    path('flutter/get/all/area/', get_all_area),
    path('flutter/register/citizen/', flutter_register_user),
    path('flutter/register/admin/', flutter_register_admin),

]