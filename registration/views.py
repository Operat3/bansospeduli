import json
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from .forms import AdminRegister, CitizenRegister
from user.models import Account, AccountMasyarakat
from staff.models import Admin
from distribusi.models import Area
from django.contrib.admin.views.decorators import staff_member_required
import random
from staff.views import admin_info
from django.contrib.auth import login
from user.views import dashboard as user_dashboard
from django.http import JsonResponse, HttpResponse


@staff_member_required
def RegisterAdmin(request):
    if request.method == "POST":
        form = AdminRegister(request.POST)
        if form.is_valid():
            admin_type = request.POST.get("admin_type", False)
            name = request.POST["name"]
            telephone = request.POST["telephone"]
            password = request.POST["password"]
            register_user = Account(is_admin=True, is_staff=False, is_citizen=False, telephone=telephone)
            register_user.set_password(password)
            register_admin = Admin(owning_user=register_user, type=admin_type, name=name)

            register_user.save()
            register_admin.save()
            login(request, register_user)
            return redirect(admin_info)

    form = AdminRegister()
    context = {"Form" : form}
    return render(request, "registeradmin.html", context)


def RegisterCitizen(request):
    if request.method == "POST":
        form = CitizenRegister(request.POST)
        
        if form.is_valid():
            admins = Admin.objects.filter(type="verification")
            area_code = request.POST.get("area", False)
            corresponding_area = Area.objects.filter(area_code=area_code)


            # Prevent account masyarakat registration when no verification admin exists
            if admins and corresponding_area:
                selected_admin = random.choice(admins)
                name = request.POST.get("name", False)
                NIK = request.POST.get("NIK", False)
                SKTMLink = request.POST.get("SKTMLink", False)
                bukuTabunganLink = request.POST.get("bukuTabunganLink", False)
                
                telephone = request.POST.get("telephone", False)
                password = request.POST.get("password", False)
                register_user = Account(is_citizen=True, telephone=telephone)
                register_user.set_password(password)
                register_citizen = AccountMasyarakat(owning_user=register_user, name=name, NIK=NIK, SKTMLink=SKTMLink, 
                bukuTabunganLink=bukuTabunganLink, partOf=corresponding_area[0], verifying_admin=selected_admin)
                
                register_user.save()
                register_citizen.save()
                login(request, register_user)

                return redirect(user_dashboard)

            else:
                return HttpResponse("Registration Process is Suspended Due To No Verification Admin Available.")
    
    form = CitizenRegister()
    areas = Area.objects.all()
    context = {"Form" : form, "Area" : areas}
    return render(request, "registercitizen.html", context)


def getAllPhoneNumber(request):
    allAccount = Account.objects.all()
    Data = []
    for account in allAccount:
        Data.append(account.telephone)

    return JsonResponse(Data, safe=False)


def get_all_phone_number(request):
    all_accounts = Account.objects.all()
    data = {"numbers": []}
    for account in all_accounts:
        data["numbers"].append(account.telephone)

    return JsonResponse(data)

def get_all_area(request):
    
    all_areas = Area.objects.all()
    data = {}
    for area in all_areas:
        data[area.area_code] = area.area_name
    return JsonResponse(data)

@csrf_exempt
def flutter_register_user(request):
    if request.method == "POST":
        raw = request.body.decode('utf-8')
        cleaned = json.loads(raw)
        admins = Admin.objects.filter(type="verification")
        corresponding_area = Area.objects.get(area_code=cleaned["areaCode"])
        if admins and corresponding_area:
            register_user = Account(is_citizen=True, telephone=cleaned["telephone"])
            register_user.set_password(cleaned["password"])
            register_citizen = AccountMasyarakat(owning_user=register_user, name=cleaned["name"], NIK=cleaned["nik"], SKTMLink=cleaned["sktmLink"], 
            bukuTabunganLink=cleaned["bukuTabunganLink"], partOf=corresponding_area, verifying_admin=random.choice(admins))
            try:
                register_user.save()
            except:
                return HttpResponse(status=409)

            register_citizen.save()
            login(request, register_user)
            return JsonResponse({"session-id": request.session.session_key, "is_admin": False, "is_citizen": True, "telephone": register_user.telephone, "is_verified": register_citizen.is_verified})
        else:
            return HttpResponse(status=500)

@csrf_exempt
def flutter_register_admin(request):
    if request.method == "POST":
        try:
            raw = request.body.decode('utf-8')
            cleaned = json.loads(raw)
            register_user = Account(is_admin=True, is_staff=False, is_citizen=False, telephone=cleaned["telephone"])
            register_user.set_password(cleaned["password"])
            register_admin = Admin(owning_user=register_user, type=cleaned["adminType"], name=cleaned["name"])

            register_user.save()
            register_admin.save()
            return HttpResponse(status=201)
        except:
            return HttpResponse(status=500)