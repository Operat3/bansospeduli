let input = document.querySelector("#telephone input")

input.addEventListener("input", verifyPhone)

function verifyPhone() {
    $.ajax({
        dataType:"json", 
        url:"/registration/api", 
        success: function(data) {
            let error=document.createElement("p")
            error.innerText = "An account with this telephone number already exist!"
            error.classList.add("error")
            if (!data.includes(input.value)) {
                input.classList.remove("invalid")
                document.querySelector(".submit").disabled=false
                let errorMessage = document.querySelector(".error")
                if (errorMessage) {
                    errorMessage.remove()
                } 
            }
            else {
                input.classList.add("invalid")
                document.querySelector(".submit").disabled=true
                let errorMessage = document.querySelector(".error")
                if (!errorMessage) {
                    $(error).insertAfter(input)
                } 
            }
        }
    })
}