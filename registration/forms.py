from django import forms
from django.forms import ModelForm
from staff.models import Admin

ADMIN_TYPES = [
            ("generator", "Generator Admin"),
            ("tracker", "Tracker Admin"),
            ("customer-service", "Customer Service"),
            ("verification", "Verification Admin")
            ]

class AdminRegister(forms.Form):
    telephone = forms.CharField(max_length=20)
    password = forms.CharField(widget=forms.PasswordInput())
    admin_type = forms.ChoiceField(choices=ADMIN_TYPES)
    name = forms.CharField(max_length=20)

class CitizenRegister(forms.Form):
    telephone = forms.CharField(max_length=20)
    password = forms.CharField(widget=forms.PasswordInput())
    name = forms.CharField(max_length=50)
    NIK = forms.CharField(max_length=16)
    SKTMLink = forms.CharField(max_length=200)
    bukuTabunganLink = forms.CharField(max_length=200)
    

