"""bansospeduli URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path

import beranda.urls as beranda
import distribusi.urls as distribusi
import laporan.urls as laporan
import registration.urls as registration
import staff.urls as staff
import user.urls as user
from beranda.views import beranda as beranda_index

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path('^$', beranda_index),
    path('beranda/', include(beranda)),
    path('distribusi/', include(distribusi)),
    path('laporan/', include(laporan)),
    path('registration/', include(registration)),
    path('staff/', include(staff)),
    path('user/', include(user)),
]
