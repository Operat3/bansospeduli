from django.db import models
import uuid, datetime
from django.utils import timezone
import pytz

# Create your models here.
def default_time(): 
    tz = pytz.timezone("Asia/Jakarta")
    return datetime.datetime.now(tz) + datetime.timedelta(days=2)

class Laporan(models.Model):
    reporting_user = models.ForeignKey("user.AccountMasyarakat", on_delete=models.CASCADE, default=None)
    reported_bansos = models.ForeignKey("distribusi.Bansos", on_delete=models.CASCADE, default=None)
    report_category = models.CharField(max_length=15, default='')
    report_message = models.TextField(blank=True)
    status = models.BooleanField(default=False)
    laporan_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    verifying_admin = models.ForeignKey("staff.Admin", on_delete=models.SET_DEFAULT, default=None)
    cooldown = models.DateTimeField(default=default_time)

