import json
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden
from .models import Laporan
from staff.models import Admin
from user.models import Account, AccountMasyarakat
from distribusi.models import Bansos, BansosGroup, BansosPreviousLocation
from .forms import ComplaintForm
from django.contrib.auth.decorators import login_required
import random
import datetime
from datetime import timezone
import math
import pytz
from django.conf import settings
from importlib import import_module
from django.views.decorators.csrf import csrf_exempt


## VIEW FUNCTIONS ##
@login_required()
def admin_view(request):
    user = request.user
    try:
        user_admin = Admin.objects.filter(
            type="customer-service").get(owning_user=user)
    except Admin.DoesNotExist:
        return HttpResponseForbidden()
    all_reports = Laporan.objects.filter(verifying_admin=user_admin)
    context = {"user": user_admin}
    context.update(extract_report_info(all_reports))
    return render(request, "laporan_admin_view.html", context)


@login_required()
def verify(request, laporan_id, status):
    status = status
    report = Laporan.objects.get(laporan_id=laporan_id)
    if request.method == "POST":
        if status == "True":
            report.status = True
            report.save()
        else:
            report.status = False
            report.save()

    return redirect("laporan-admin")


@login_required()
def choose_bansos(request):
    user = request.user
    try:
        user_account = AccountMasyarakat.objects.get(owning_user=user)
        if not user_account.is_verified:
            return redirect("/")
    except AccountMasyarakat.DoesNotExist:
        return HttpResponseForbidden()
    arrived_bansos_group = BansosGroup.objects.filter(status="received")
    all_bansos = Bansos.objects.filter(received_by=user_account).filter(
        owning_group__in=arrived_bansos_group)
    if all_bansos.count():
        context = {"reported": {}, "unreported": {}, "user": user_account}
        for bansos in all_bansos:
            # {[id]:[info]}
            # {"38912-dy21782-..." : {kelurahan:, bentuk_bantuan:{"minyak":300}}}
            if bansos.is_reported:
                context["reported"][bansos.id_bansos] = get_bansos_info(
                    bansos.owning_group)
                context["reported"][bansos.id_bansos]["is_reported"] = bansos.is_reported
                laporan = Laporan.objects.get(reported_bansos=bansos)
                context["reported"][bansos.id_bansos]["laporan"] = {
                    "category": laporan.report_category, "message": laporan.report_message,
                    "status": str(laporan.status), "id": laporan.laporan_id
                }
            else:
                context["unreported"][bansos.id_bansos] = get_bansos_info(
                    bansos.owning_group)
                context["unreported"][bansos.id_bansos]["is_reported"] = bansos.is_reported
            # TODO: sort by date?, give cooldown period for report?
        return render(request, "laporan_lapor.html", context)

    else:
        context = {"action": "empty", "user": user_account}
        return render(request, "laporan_intermezzo.html", context)


@login_required()
def generate_report(request, id_bansos):
    user = request.user
    try:
        user_account = AccountMasyarakat.objects.get(owning_user=user)
        if not user_account.is_verified:
            return redirect("/")
    except AccountMasyarakat.DoesNotExist:
        return HttpResponseForbidden()
    bansos = Bansos.objects.get(id_bansos=id_bansos)
    # make sure bansos was received by the user requesting
    try:
        laporan = Laporan.objects.get(reported_bansos=bansos)
        return redirect(f"/laporan/update/{laporan.laporan_id}")
    except Laporan.DoesNotExist:
        pass
    if bansos.received_by == user_account:
        if request.method == "POST":
            form = ComplaintForm(request.POST)
            report_category = request.POST["complaint_category"]
            report_message = request.POST["complaint"]
            admins = Admin.objects.filter(type="customer-service")
            if not admins.count():
                return HttpResponse("The Report System is Suspended due to No Customer Service Available")
            random_index = random.randint(0, admins.count()-1)
            bansos.is_reported = True
            bansos.save()
            laporan = Laporan(
                reporting_user=user_account,
                reported_bansos=bansos,
                report_category=report_category,
                report_message=report_message,
                verifying_admin=admins[random_index]
            )
            laporan.save()
            context = {"action": "saved", "user": user_account}
            return render(request, "laporan_intermezzo.html", context)

        form = ComplaintForm()
        context = {"form": form, "title": "Generate Report",
                   "bansos": bansos, "action": "Generating", "action2": "Send Report", "user": user_account}
        return render(request, "laporan_generate.html", context)

    return redirect("laporan-lapor")


@login_required()
def update_report(request, laporan_id):
    user = request.user
    try:
        user_account = AccountMasyarakat.objects.get(owning_user=user)
        if not user_account.is_verified:
            return redirect("/")
    except AccountMasyarakat.DoesNotExist:
        return HttpResponseForbidden()

    laporan = Laporan.objects.get(laporan_id=laporan_id)
    if laporan.reporting_user == user_account and not laporan.status:
        tz = pytz.timezone("Asia/Jakarta")
        today = datetime.datetime.now(tz)
        cooldown_time = laporan.cooldown
        cooldown_finished = math.floor(
            (cooldown_time - today).total_seconds()) <= 0
        if cooldown_finished:
            if request.method == "POST":
                form = ComplaintForm(request.POST)
                laporan.report_category = request.POST["complaint_category"]
                laporan.report_message = request.POST["complaint"]
                laporan.cooldown = datetime.datetime.now() + datetime.timedelta(days=1, hours=17)
                laporan.save()
                context = {"action": "saved", "user": user}
                return render(request, "laporan_intermezzo.html", context)
            else:
                form = ComplaintForm()
                form.fields["complaint_category"].initial = laporan.report_category
                form.fields["complaint"].initial = laporan.report_message
                context = {"form": form, "title": "Update Report", "bansos": laporan.reported_bansos,
                           "action": "Updating", "action2": "Update Report", "user": user_account}
                return render(request, "laporan_generate.html", context)

    return redirect("laporan-lapor")

## APIS ##


def all_assigned_reports(request):
    if request.is_ajax():
        data = {}
        user = request.user
        try:
            user_admin = Admin.objects.get(owning_user=user)
        except Admin.DoesNotExist:
            return HttpResponseForbidden()
        all_laporan = Laporan.objects.filter(verifying_admin=user_admin)
        for laporan in all_laporan:
            id = str(laporan.laporan_id)
            data[id] = {}
            data[id]["user"] = {
                "NIK": laporan.reporting_user.NIK, "name": laporan.reporting_user.name}
            data[id]["category"] = laporan.report_category
            data[id]["bansos"] = {}
            data[id]["bansos"]["id"] = laporan.reported_bansos.id_bansos
            data[id]["bansos"]["bentuk_bantuan"] = {}
            data[id]["bansos"]["bentuk_bantuan"]["bentuk"] = laporan.reported_bansos.owning_group.bentuk_bantuan
            data[id]["bansos"]["bentuk_bantuan"]["satuan"] = laporan.reported_bansos.owning_group.satuan_bantuan
            data[id]["status"] = laporan.status
            data[id]["message"] = laporan.report_message
        return JsonResponse(data)

    return HttpResponseForbidden()


def laporan_info(request):
    if request.is_ajax():
        data = {}
        user = request.user
        try:
            user_account = AccountMasyarakat.objects.get(
                owning_user=request.user)
        except AccountMasyarakat.DoesNotExist:
            return HttpResponseForbidden()
        all_laporan = Laporan.objects.filter(
            reporting_user=user_account).filter(status=False)
        data = {}
        for laporan in all_laporan:
            data[str(laporan.laporan_id)] = laporan.cooldown
        return JsonResponse(data)

    return HttpResponseForbidden()


def get_user_bansos(request):
    if request.is_ajax():
        try:
            user_account = AccountMasyarakat.objects.get(
                owning_user=request.user)
        except AccountMasyarakat.DoesNotExist:
            return HttpResponseForbidden()
        arrived_bansos_group = BansosGroup.objects.filter(status="received")
        all_bansos = Bansos.objects.filter(received_by=user_account).filter(
            owning_group__in=arrived_bansos_group)
        data = {}
        for bansos in all_bansos:
            # {[id]:[info]}
            # {"38912-dy21782-..." : {kelurahan:, bentuk_bantuan:{"minyak":300}}}
            data[str(bansos.id_bansos)] = get_bansos_info(
                bansos.owning_group)
            data[str(bansos.id_bansos)]["is_reported"] = bansos.is_reported

        return JsonResponse(data)

    return HttpResponseForbidden()

## HELPER FUNCTIONS ##


def get_bansos_info(bansos):
    # get all info attributes of the bansos from the bansos group
    bansos_attributes = [
        'kelurahan', 'kecamatan',
        'provinsi', 'bentuk_bantuan',
        'responsible_admin'
    ]
    info = {}
    for attr in bansos_attributes:
        if attr == "bentuk_bantuan":
            # bentuk_bantuan:{'Bakso': '50', 'Kerupuk': '100'} => bentuk_bantuan:{ '': {jumlah: , satuan:} }
            bantuan = getattr(bansos, attr)
            satuan = getattr(bansos, "satuan_bantuan")
            context = {}
            for jenis, jumlah in bantuan.items():
                context[jenis] = {}
                context[jenis]["jumlah"] = jumlah
                context[jenis]["satuan"] = satuan[jenis]

            info["bentuk_bantuan"] = context

        else:
            info[attr] = getattr(bansos, attr)
            if attr == 'responsible_admin':
                admin = info[attr]
                info[attr] = admin.name

    try:
        arrived_timestamp = BansosPreviousLocation.objects.filter(
            bansos_group=bansos).get(status="received")
        if arrived_timestamp:
            timestamp = arrived_timestamp.time_stamp
        info["timestamp"] = timestamp.strftime("%m/%d/%Y, %H:%M:%S")
    except:
        pass

    return info


def extract_report_info(all_reports):
    data = {}
    data["verified"] = {}
    data["unverified"] = {}
    for report in all_reports:
        if report.status:
            data["verified"][report.laporan_id] = {}
            data["verified"][report.laporan_id]["report"] = report
            data["verified"][report.laporan_id]["bansosinfo"] = {}
            bentuk_bantuan = report.reported_bansos.owning_group.bentuk_bantuan
            satuan_bantuan = report.reported_bansos.owning_group.satuan_bantuan
            for bantuan, jumlah in bentuk_bantuan.items():
                data["verified"][report.laporan_id]["bansosinfo"] = {}
                data["verified"][report.laporan_id]["bansosinfo"][bantuan] = {
                    "jumlah": jumlah, "satuan": satuan_bantuan[bantuan]}
        else:
            data["unverified"][report.laporan_id] = {}
            data["unverified"][report.laporan_id]["report"] = report
            data["unverified"][report.laporan_id]["bansosinfo"] = {}
            bentuk_bantuan = report.reported_bansos.owning_group.bentuk_bantuan
            satuan_bantuan = report.reported_bansos.owning_group.satuan_bantuan
            for bantuan, jumlah in bentuk_bantuan.items():
                data["unverified"][report.laporan_id]["bansosinfo"] = {}
                data["unverified"][report.laporan_id]["bansosinfo"][bantuan] = {
                    "jumlah": jumlah, "satuan": satuan_bantuan[bantuan]}
    return data

## FLUTTER API ##


def get_user_bansos_info(request, bansosid):
    bansos = Bansos.objects.get(id_bansos=bansosid)
    info = get_bansos_info(bansos.owning_group)
    return JsonResponse(info)


def get_all_user_bansos(request, sessionid):
    telephone = get_credentials(sessionid)
    user = Account.objects.get(telephone=telephone).accountmasyarakat
    arrived_bansos_group = BansosGroup.objects.filter(status="received")
    all_bansos = Bansos.objects.filter(received_by=user).filter(
        owning_group__in=arrived_bansos_group)

    all_bansos_id = []
    for bansos in all_bansos:
        if not bansos.is_reported:
            all_bansos_id.append(bansos.id_bansos)

    jsonified = {"data": all_bansos_id}
    return JsonResponse(jsonified)


def get_all_user_laporan(request, sessionid):
    telephone = get_credentials(sessionid)
    user = Account.objects.get(telephone=telephone).accountmasyarakat
    all_laporans = Laporan.objects.filter(reporting_user=user)
    data = {}
    for laporan in all_laporans:
        data[str(laporan.laporan_id)] = laporan.status

    return JsonResponse(data)


def get_user_laporan_info(request, laporanid):
    laporan = Laporan.objects.get(laporan_id=laporanid)
    data = {"laporan_id": laporan.laporan_id, "report_category": laporan.report_category, "report_message": laporan.report_message, "reported_bansos_id": laporan.reported_bansos.id_bansos,
            "cooldown": laporan.cooldown.strftime("%m/%d/%Y, %H:%M:%S"), "bansos_info": get_bansos_info(laporan.reported_bansos.owning_group)}
    return JsonResponse(data)


@csrf_exempt
def post_user_laporan(request, sessionid):
    if request.method == "POST":
        data = request.body.decode("utf-8")
        laporan_data = json.loads(data)
        report_message = laporan_data["report_message"]
        report_category = laporan_data["report_category"]
        reported_bansos = Bansos.objects.get(
            id_bansos=laporan_data["reported_bansos_id"])
        reporting_user = Account.objects.get(
            telephone=get_credentials(sessionid)).accountmasyarakat
        admins = Admin.objects.filter(type="customer-service")
        if not admins.count():
            return HttpResponse(status=503)
        random_index = random.randint(0, admins.count()-1)
        reported_bansos.is_reported = True
        reported_bansos.save()
        laporan = Laporan(
            reporting_user=reporting_user,
            reported_bansos=reported_bansos,
            report_category=report_category,
            report_message=report_message,
            verifying_admin=admins[random_index]
        )
        laporan.save()
        return HttpResponse(status=201)


@csrf_exempt
def patch_user_laporan(request, laporanid):
    laporan = Laporan.objects.get(laporan_id=laporanid)
    tz = pytz.timezone("Asia/Jakarta")
    today = datetime.datetime.now().astimezone(tz=timezone.utc) + datetime.timedelta(hours=7)
    cooldown_time = laporan.cooldown
    cooldown_finished = math.floor(
        (cooldown_time - today).total_seconds()) <= 0
    if cooldown_finished:
        raw_data = request.body.decode("utf-8")
        laporan_data = json.loads(raw_data)
        if request.method == "POST":
            laporan.report_message = laporan_data["report_message"]
            laporan.report_category = laporan_data["report_category"]
            laporan.cooldown = datetime.datetime.now(tz) + datetime.timedelta(days=2)
            laporan.save()
            return HttpResponse(status=204) 
    
    return HttpResponse(status=500)

def get_all_user_laporan_admin(request, sessionid):
    user = Account.objects.get(telephone=get_credentials(sessionid))
    admin = user.admin
    all_laporans = Laporan.objects.filter(verifying_admin=admin)
    data = {}
    for laporan in all_laporans:
        data[str(laporan.laporan_id)] = laporan.status

    return JsonResponse(data)

@csrf_exempt
def patch_user_laporan_admin(request, laporanid):
    if request.method == "POST":
        data = request.body.decode("utf-8")
        processed_data = json.loads(data)
        status = processed_data["status"]
        laporan = Laporan.objects.get(laporan_id=laporanid)
        if status:
            laporan.status = True
        else:
            laporan.status = False
        laporan.save()
        return HttpResponse(status=204)

    return HttpResponse(status=500)



def get_credentials(id):
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(id)
    return session.get("_auth_user_id")
