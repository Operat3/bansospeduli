from django.http import response
from django.test import TestCase
from django.contrib.auth import login
from django.http.request import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from user.models import Account, AccountMasyarakat
from staff.models import Admin
from laporan.models import Laporan
from distribusi.models import *
import datetime

class LaporanTestCase(TestCase):
    # check login required
    def test_admin_view_loginrequired(self):
        response = Client().get("/laporan/verify/")
        self.assertEqual(response.status_code, 302)
    
    def test_admin_verify_loginrequired(self):
        
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        new_account = Account(telephone="081315868053")
        new_account.set_password("hello")
        new_account.save()

        masyarakat = AccountMasyarakat(
            owning_user=new_account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin
        )
        masyarakat.save()
        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        new_account3 = Account(telephone="0812")
        new_account3.set_password("hello")
        new_account3.save()

        customer_service = Admin(owning_user=new_account3,type="customer-service",name="Azizi")
        customer_service.save()

        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            )

        new_laporan.save()

        response = Client().get("/laporan/verify/"+str(new_laporan.laporan_id)+"/accepted=True")
        self.assertEqual(response.status_code, 302)
    
    def test_choose_bansos_loginrequired(self):
        response = Client().get("/laporan/")
        self.assertEqual(response.status_code, 302)
    
    def test_generate_report_loginrequired(self):

        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        new_account = Account(telephone="081315868053")
        new_account.set_password("hello")
        new_account.save()

        masyarakat = AccountMasyarakat(
            owning_user=new_account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin
        )
        masyarakat.save()
        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=False)
        bansos.save()

        response = Client().get("/laporan/generate/" + str(bansos.id_bansos))
        self.assertEqual(response.status_code, 302)
    
    def test_update_report_loginrequired(self):

        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        new_account = Account(telephone="081315868053")
        new_account.set_password("hello")
        new_account.save()

        masyarakat = AccountMasyarakat(
            owning_user=new_account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin
        )
        masyarakat.save()
        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        new_account3 = Account(telephone="0812")
        new_account3.set_password("hello")
        new_account3.save()

        customer_service = Admin(owning_user=new_account3,type="customer-service",name="Azizi")
        customer_service.save()

        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            )

        new_laporan.save()

        response = Client().get("/laporan/update/" + str(new_laporan.laporan_id))
        self.assertEqual(response.status_code, 302)
    
    def test_choose_bansos(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        self.account = Account(telephone="081315868053")
        self.account.set_password("hello")
        self.account.save()

        masyarakat = AccountMasyarakat(
            owning_user=self.account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        self.client.login(telephone="081315868053", password="hello")

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        new_account3 = Account(telephone="0812")
        new_account3.set_password("hello")
        new_account3.save()

        customer_service = Admin(owning_user=new_account3,type="customer-service",name="Azizi")
        customer_service.save()

        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            )

        new_laporan.save()

        response = self.client.get("/laporan/")
        self.assertEqual(response.status_code, 200)

    def test_generate_bansos(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        self.account = Account(telephone="081315868053")
        self.account.set_password("hello")
        self.account.save()

        masyarakat = AccountMasyarakat(
            owning_user=self.account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        self.client.login(telephone="081315868053", password="hello")

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        new_account3 = Account(telephone="0812")
        new_account3.set_password("hello")
        new_account3.save()

        customer_service = Admin(owning_user=new_account3,type="customer-service",name="Azizi")
        customer_service.save()

        response = self.client.get("/laporan/generate/" + str(bansos.id_bansos))
        self.assertEqual(response.status_code, 200)

    def test_update_report(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        self.account = Account(telephone="081315868053")
        self.account.set_password("hello")
        self.account.save()

        masyarakat = AccountMasyarakat(
            owning_user=self.account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        self.client.login(telephone="081315868053", password="hello")

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        new_account3 = Account(telephone="0812")
        new_account3.set_password("hello")
        new_account3.save()

        customer_service = Admin(owning_user=new_account3,type="customer-service",name="Azizi")
        customer_service.save()

        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            cooldown = datetime.datetime.now() + datetime.timedelta(days=-1)
            )

        new_laporan.save()

        response = self.client.get("/laporan/update/" + str(new_laporan.laporan_id))
        self.assertEqual(response.status_code, 200)

    def test_admin_view(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        new_account = Account(telephone="081315868053")
        new_account.set_password("hello")
        new_account.save()

        masyarakat = AccountMasyarakat(
            owning_user=new_account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        self.client.login(telephone="081315868053", password="hello")

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        self.account = Account(telephone="0812")
        self.account.set_password("hello")
        self.account.save()

        customer_service = Admin(owning_user=self.account,type="customer-service",name="Azizi")
        customer_service.save()

        self.client.login(telephone= "0812", password="hello")

        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            cooldown = datetime.datetime.now() + datetime.timedelta(days=-1)
            )

        new_laporan.save()

        response = self.client.get("/laporan/verify/")
        self.assertEqual(response.status_code, 200)

    def test_forbidden_verify(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        self.account = Account(telephone="081315868053")
        self.account.set_password("hello")
        self.account.save()

        masyarakat = AccountMasyarakat(
            owning_user=self.account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        self.client.login(telephone="081315868053", password="hello")

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        new_account3 = Account(telephone="0812")
        new_account3.set_password("hello")
        new_account3.save()

        customer_service = Admin(owning_user=new_account3,type="customer-service",name="Azizi")
        customer_service.save()

        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            cooldown = datetime.datetime.now() + datetime.timedelta(days=-1)
            )

        new_laporan.save()

        response = self.client.get("/laporan/verify/")
        self.assertEqual(response.status_code, 403)
    
    def test_forbidden_choose(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        new_account = Account(telephone="081315868053")
        new_account.set_password("hello")
        new_account.save()

        masyarakat = AccountMasyarakat(
            owning_user=new_account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        self.client.login(telephone="081315868053", password="hello")

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        self.account = Account(telephone="0812")
        self.account.set_password("hello")
        self.account.save()

        customer_service = Admin(owning_user=self.account,type="customer-service",name="Azizi")
        customer_service.save()

        self.client.login(telephone= "0812", password="hello")

        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            cooldown = datetime.datetime.now() + datetime.timedelta(days=-1)
            )

        new_laporan.save()

        response = self.client.get("/laporan/")
        self.assertEqual(response.status_code, 403)

    def test_forbidden_generate(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        new_account = Account(telephone="081315868053")
        new_account.set_password("hello")
        new_account.save()

        masyarakat = AccountMasyarakat(
            owning_user=new_account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        self.account = Account(telephone="0812")
        self.account.set_password("hello")
        self.account.save()

        customer_service = Admin(owning_user=self.account,type="customer-service",name="Azizi")
        customer_service.save()

        self.client.login(telephone= "0812", password="hello")

        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            cooldown = datetime.datetime.now() + datetime.timedelta(days=-1)
            )

        new_laporan.save()

        response = self.client.get("/laporan/generate/" + str(bansos.id_bansos))
        self.assertEqual(response.status_code, 403)

    def test_bansos_info_ajax(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        self.account = Account(telephone="081315868053")
        self.account.set_password("hello")
        self.account.save()

        masyarakat = AccountMasyarakat(
            owning_user=self.account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        self.client.login(telephone="081315868053", password="hello")

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        response = self.client.get("/laporan/user-bansos/",  **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)

    def test_report_cooldown_ajax(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        self.account = Account(telephone="081315868053")
        self.account.set_password("hello")
        self.account.save()

        masyarakat = AccountMasyarakat(
            owning_user=self.account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        self.client.login(telephone="081315868053", password="hello")

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        new_account3 = Account(telephone="0812")
        new_account3.set_password("hello")
        new_account3.save()

        customer_service = Admin(owning_user=new_account3,type="customer-service",name="Azizi")
        customer_service.save()

        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            cooldown = datetime.datetime.now() + datetime.timedelta(days=-1)
            )

        new_laporan.save()

        response = self.client.get("/laporan/get-all-laporan/",  **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
    
    def test_all_laporan_admin_ajax(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        generator_account = Account(telephone='082312330001')
        generator_account.set_password('helloworld')
        generator_admin = Admin(owning_user=generator_account, type='generator', name='Winaldo')
        generator_account.save()
        generator_admin.save()

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='received',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )

        bansos_to_test.save()

        new_account2 = Account(telephone="0811")
        new_account2.set_password("hello")
        new_account2.save()

        verifying_admin = Admin(owning_user=new_account2,type="tracker",name="Aziz")
        verifying_admin.save()

        self.account = Account(telephone="081315868053")
        self.account.set_password("hello")
        self.account.save()

        masyarakat = AccountMasyarakat(
            owning_user=self.account, 
            name="Rashad",
            NIK="1111",
            SKTMLink="google.com",
            bukuTabunganLink="google.com",
            partOf=new_area,
            verifying_admin=verifying_admin,
            is_verified=True,
        )
        masyarakat.save()

        bansos = Bansos(owning_group=bansos_to_test, received_by=masyarakat, is_reported=True)
        bansos.save()

        new_account3 = Account(telephone="0812")
        new_account3.set_password("hello")
        new_account3.save()

        customer_service = Admin(owning_user=new_account3,type="customer-service",name="Azizi")
        customer_service.save()

        self.client.login(telephone="0812", password="hello")
        
        new_laporan = Laporan(
            reporting_user=masyarakat, 
            reported_bansos=bansos, 
            report_category="insufficient",
            report_message="WHAT",
            status="False",
            verifying_admin=customer_service,
            cooldown = datetime.datetime.now() + datetime.timedelta(days=-1)
            )

        new_laporan.save()

        response = self.client.get("/laporan/get-assigned-laporan/",  **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)