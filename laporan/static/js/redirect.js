let countdown = setInterval(function() {
    let timer = document.querySelector(".countdown")
    if (parseInt(timer.innerText) > 1) {
        timer.innerText = parseInt(timer.innerText) - 1
    } else {
        clearInterval(countdown)
        let is_update = document.querySelector(".update")
        if (is_update) {
            location.replace("/laporan")
        } else {
        location.replace("/")
        }
    }
}, 1000)