setInterval(ajaxCall, 600000)
function ajaxCall() {
    $.ajax({
        dataType: "json",
        url: "/laporan/get-assigned-laporan",
        success: function(data) {
            current_laporan = document.querySelectorAll(".laporan-id")
            current_laporan_id = []
            if (current_laporan) {
                for (laporan of current_laporan) {
                    try {
                        current_laporan_id.push(laporan.innerText.substring(9, 45));
                    } catch {
                        current_laporan_id.push("-")
                    }
                }
            } else {
                current_laporan_id = []
            }
            deleted_laporan = check_deleted_laporan(current_laporan_id, Object.keys(data));
            new_laporan = check_new_laporan(current_laporan_id, Object.keys(data));
            
            if (deleted_laporan.length || new_laporan.length) {
                location.reload()
            } 
        }
    })
}
function check_deleted_laporan(all_laporan, requested_laporan) {
    let deleted_laporan = all_laporan.filter((x) => !requested_laporan.includes(x));
    return deleted_laporan;
  }
  function check_new_laporan(all_laporan, requested_laporan) {
    let new_laporan = requested_laporan.filter((x) => !all_laporan.includes(x));
    return new_laporan;
  }