ajaxCall();

function ajaxCall() {
  $.ajax({
    dataType: "json",
    url: "/laporan/get-all-laporan",
    success: function (data) {
      let all_cooldowns = {};
      for (const key in data) {
        let date = new Date(data[key]);
        all_cooldowns[key] = date;
      }
      updateCooldown(all_cooldowns);
    },
  });
}

function updateCooldown(all_laporan) {
  for (const id in all_laporan) {
    let countdown = document.getElementById(`${id}`);
    timer(countdown, all_laporan[id], id);
  }
}
function timer(element, cooldown, id) {
  let currDate = new Date();
  let difference = cooldown - currDate;

  let seconds = Math.floor(difference / 1000);
  let minutes = Math.floor(seconds / 60);
  let hours = Math.floor(minutes / 60);
  let days = Math.floor(hours / 24);

  hours %= 24;
  minutes %= 60;
  seconds %= 60;

  if (days >= 1 && days > 0) {
    setTimeout(function () {
      timer(element, cooldown, id);
    }, 300000);
    element.innerText = `${days} days`;
  } else if (hours <= 23 && hours > 0) {
    setTimeout(function () {
      timer(element, cooldown, id);
    }, 1000);
    element.innerText = `${hours.toString().padStart(2, "0")}:${minutes
      .toString()
      .padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;
  } else if (minutes <= 60 && minutes > 0) {
    setTimeout(function () {
      timer(element, cooldown, id);
    }, 1000);
    element.innerText = `${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}`;
  } else if (seconds <= 60 && seconds > 0) {
    setTimeout(function () {
      timer(element, cooldown, id);
    }, 1000);
    element.innerText = `${seconds} seconds`;
  } else {
    let button = element.parentNode;
    button.querySelector(".lock").remove();
    button.innerText = "Update Report";
    setTimeout(() => {
      button.classList.remove("disabled");
      button.setAttribute("href", `/laporan/update/${id}`);
    }, 1000);
  }
}
