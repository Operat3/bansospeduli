let button = document.querySelector(".show-reported")
if (button) {
  button.addEventListener("click", show);
}
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function show() {
  let reported = document.querySelector(".reported");
  let button = document.querySelector(".show-reported");
  if (
    !reported.classList.contains(!show) &&
    reported.classList.contains("hidden")
  ) {
    button.innerText = "Hide Previously Reported Bansos";
    button.classList.remove("btn-primary")
    button.classList.add("btn-outline-primary")
    reported.classList.add("show");
    reported.classList.remove("hidden");
  } else {
    button.innerText = "Show Previously Reported Bansos";
    reported.classList.add("hidden");
    button.classList.remove("btn-outline-primary")
    button.classList.add("btn-primary")
    reported.classList.remove("show");
  }
}