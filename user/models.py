from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
import uuid

class MyAccountManager(BaseUserManager):
    def create_user(self, telephone, password=None):
        if not telephone:
            raise ValueError("Users must have a telephone number")
        
        user = self.model(
            telephone=telephone,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self, telephone, password):
        user = self.create_user(
            telephone=telephone,
            password=password,
        )
        user.is_staff = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):
    telephone = models.CharField(max_length=20,primary_key=True, blank=True)
    is_staff = models.BooleanField(default=False)
    is_citizen = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    USERNAME_FIELD = 'telephone'
    REQUIRED_FIELDS = []
    
    objects = MyAccountManager()

    def __str__(self):
        return self.telephone
    
    def has_perm(self, perm, obj=None):
        return self.is_staff

    def has_module_perms(self, app_label):
        return True

# Create your models here.
class AccountMasyarakat(models.Model):
    owning_user = models.OneToOneField(Account, on_delete=models.CASCADE, primary_key=True)
    is_verified = models.BooleanField(default=False)
    name = models.CharField(max_length=50)
    NIK = models.CharField(max_length=16)
    SKTMLink = models.CharField(max_length=200)
    bukuTabunganLink = models.CharField(max_length=200)
    partOf = models.ForeignKey("distribusi.Area", 
        on_delete=models.RESTRICT, 
        default='NULL'
    )
    verifying_admin = models.ForeignKey("staff.Admin", on_delete=models.SET_DEFAULT, default=None)


