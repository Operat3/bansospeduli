from django.contrib.auth import login
from django.http.request import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from user.models import Account, AccountMasyarakat
from staff.models import Admin
from distribusi.models import Area, BansosGroup

class UserTestCase(TestCase):
    # Check login required
    def test_dashboard_is_login_required(self):
        response = Client().get('/user/dashboard/')
        self.assertEqual(response.status_code, 302)
    
    def test_mybansos_is_login_required(self):
        response = Client().get('/user/dashboard/mybansos')
        self.assertEqual(response.status_code, 302)
    
    def test_history_is_login_required(self):
        response = Client().get('/user/dashboard/history')
        self.assertEqual(response.status_code, 302)

    # Check 200 with login (User Endpoints Check)
    def test_dashboard(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        account_admin = Account(telephone='082312330002')
        account_admin.set_password('helloworld')
        verifying_admin = Admin(owning_user=account_admin, type='verification', name='Winaldo')
        account_admin.save()
        verifying_admin.save()

        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        account_masyarakat = AccountMasyarakat(owning_user=self.account, is_verified=True, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
        self.account.save()
        account_masyarakat.save()
        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/user/dashboard/')
        self.assertEqual(response.status_code, 200)
    
    def test_mybansos(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        account_admin = Account(telephone='082312330002')
        account_admin.set_password('helloworld')
        verifying_admin = Admin(owning_user=account_admin, type='verification', name='Winaldo')
        account_admin.save()
        verifying_admin.save()

        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        account_masyarakat = AccountMasyarakat(owning_user=self.account, is_verified=True, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
        self.account.save()
        account_masyarakat.save()
        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/user/dashboard/mybansos')
        self.assertEqual(response.status_code, 200)
  
    def test_history(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        account_admin = Account(telephone='082312330002')
        account_admin.set_password('helloworld')
        verifying_admin = Admin(owning_user=account_admin, type='verification', name='Winaldo')
        account_admin.save()
        verifying_admin.save()

        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        account_masyarakat = AccountMasyarakat(owning_user=self.account, is_verified=True, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
        self.account.save()
        account_masyarakat.save()
        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/user/dashboard/history')
        self.assertEqual(response.status_code, 200)
    
    
    # TEST FORBIDDEN 403
    def test_dashboard_by_admin(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/user/dashboard/')
        self.assertEqual(response.status_code, 403)
 
    def test_mybansos_by_admin(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/user/dashboard/mybansos')
        self.assertEqual(response.status_code, 403)

    def test_history_by_admin(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/user/dashboard/history')
        self.assertEqual(response.status_code, 403)