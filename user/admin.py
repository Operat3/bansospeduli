from django.contrib import admin
from .models import Account, AccountMasyarakat
from django.contrib.auth.admin import UserAdmin

# class AccountAdmin(UserAdmin):

#     fieldsets = (
#         (None, {'fields': ('telephone',)}),
#         ('Permissions', {'fields': ('is_staff','is_citizen', 'is_admin')}),
#     )
#     add_fieldsets = (
#         (None, {
#             'classes': ('wide',),
#             'fields': ('telephone', 'is_staff','is_citizen', 'is_admin'),
#         }),
#     )

#     ordering = ('telephone','is_staff', 'is_citizen')
#     list_display = ('telephone','is_staff', 'is_citizen')
#     search_fields = ('telephone','is_staff', 'is_citizen')
#     readonly_fields = ()

    
#     filter_horizontal = ()
#     list_filter = ()

    
    

# Register your models here.
admin.site.register(Account)
admin.site.register(AccountMasyarakat)

