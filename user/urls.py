from django.urls import path
from django.contrib.auth import views as auth_views
from django.conf.urls import url

from laporan.views import get_bansos_info
from .views import *

urlpatterns = [
    path('flutter/test/', flutter_test),
    path('flutter/login/', flutter_user_login),
    path('dashboard/',dashboard, name = 'dashboard'),
    path('dashboard/mybansos',mybansos,name = 'mybansos'),
    path('',dashboard, name = 'dashboard'), 
    path('dashboard/history',history,name ='history'), 
    path('edit_note', edit_note, name='edit_note'),
    path('flutter/login/', flutter_user_login),
    path('flutter/logout/', flutter_user_logout),
    path('flutter-get-user-info/',flutter_get_user_info),
    path('flutter-update-user/',flutter_update_user),
    path('flutter-user-bansos/',get_user_bansos),
    path('flutter-user-info-bansos/',get_user_bansos_info),
    path('flutter-user-detail-bansos/',user_bansos_from_id)
]
