from django.shortcuts import redirect, render
from django.contrib.auth import authenticate,logout,login
from distribusi.models import *
from django.core.paginator import Paginator
from django.contrib.auth import logout
import uuid
from staff.models import Admin
from .models import *
from laporan.forms import LoginForm
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseForbidden, JsonResponse
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.contrib.sessions.backends.db import SessionStore
from django.views.decorators.csrf import csrf_exempt
import json
from django.conf import settings
from importlib import import_module

@login_required()
def dashboard(request):
    # Add additional variable if admin is a tracking admin and is currently occupied
    user = request.user
    try:
        user = AccountMasyarakat.objects.get(owning_user=user)
    
    except AccountMasyarakat.DoesNotExist:
        return HttpResponseForbidden()

    akun = AccountMasyarakat.objects.get(owning_user = user )
    account = akun
    bansos = Bansos.objects.filter(received_by = request.user.telephone)
    response = {'bansos': bansos, 'account':account}
    
    return render(request, 'dashboard.html', response)

@login_required()
def mybansos(request):
    user = request.user
    try:
        user = AccountMasyarakat.objects.get(owning_user=user)
    
    except AccountMasyarakat.DoesNotExist:
        return HttpResponseForbidden()
    
    akun = AccountMasyarakat.objects.get(owning_user = user )
    user = request.user
    bansosgrup = BansosGroup.objects.filter(status = 'queued') | BansosGroup.objects.filter(status = 'sent')
    que = request.GET.get('queued')
    sent = request.GET.get('sent')
    
    if que == 'queued':
        bansos_queue = Bansos.objects.filter(owning_group__status = 'queued',received_by = user.telephone)
        bansosgrup = BansosGroup.objects.filter(status = 'queued').order_by('id_bansos_group')
        bansos_queue_page = Paginator(bansosgrup,1)
        page_num = request.GET.get('page')
        page = bansos_queue_page.get_page(page_num)
        account = akun
        bansosprev = BansosPreviousLocation.objects.filter(bansos_group__status = 'queued')
        response = {'page' : page,  'bansosqueue': bansos_queue, 'bansos_queue_page' : bansos_queue_page, 'prevloc': bansosprev,'account':account}
        return render(request,'mybansos.html',response)
    
    elif sent == 'sent':
        bansos_queue = Bansos.objects.filter(owning_group__status = 'sent',received_by = user.telephone)
        bansosgrup = BansosGroup.objects.filter(status = 'sent').order_by('id_bansos_group')
        bansos_queue_page = Paginator(bansosgrup,1)
        page_num = request.GET.get('page')
        page = bansos_queue_page.get_page(page_num)
        account = akun
        bansosprev = BansosPreviousLocation.objects.filter(bansos_group__status = 'sent')
        response = {'page' : page,  'bansosqueue': bansos_queue, 'bansos_queue_page' : bansos_queue_page,'prevloc': bansosprev,'account':account}
        return render(request,'mybansos.html',response)
    
    else:
        bansos_queue = Bansos.objects.filter(owning_group__status = 'queued',received_by = user.telephone) | Bansos.objects.filter(owning_group__status = 'sent',received_by = user.telephone)
        bansosgrup = BansosGroup.objects.filter(status = 'queued').order_by('id_bansos_group') | BansosGroup.objects.filter(status = 'sent').order_by('id_bansos_group')
        # prevloc = BansosPreviousLocation.objects.filter(bansos_group = Bansos.objects.filter(owning_group__status = 'queued').values(uuid.UUID('owning_group')).first()).first()
        bansos_queue_page = Paginator(bansosgrup,1)
        page_num = request.GET.get('page')
        page = bansos_queue_page.get_page(page_num)
        account = akun
        bansosprev = BansosPreviousLocation.objects.filter(bansos_group__status = 'queued') | BansosPreviousLocation.objects.filter(bansos_group__status = 'sent')
        response = {'page' : page,  'bansosqueue': bansos_queue, 'bansos_queue_page' : bansos_queue_page,'prevloc':bansosprev,'account': account}
        return render(request,'mybansos.html',response)
    

@login_required()
def history(request): 
    user = request.user

    try:
        user = AccountMasyarakat.objects.get(owning_user=user)

    except AccountMasyarakat.DoesNotExist:
        return HttpResponseForbidden()

    user = request.user
    akun = AccountMasyarakat.objects.get(owning_user = user )
    receivedBansos = Bansos.objects.filter(owning_group__status = 'received', received_by = user.telephone)
    previousLocation = BansosPreviousLocation.objects.filter(bansos_group__status = 'received').all()
    response = {'receivedBansos': receivedBansos,'previousLocation':previousLocation,'account':akun}
    return render(request, 'history.html',response)

def edit_note(request):
    if (request.method == "POST"):
        nomor = request.POST.get("id")
        sktm = request.POST.get("sktm")
        buku = request.POST.get("buku")

        akun = AccountMasyarakat.objects.get(owning_user__telephone = nomor)
        akun.SKTMLink = sktm
        akun.bukuTabunganLink = buku
        # akun.is_verified = False
        akun.save()
    return HttpResponseRedirect("/user/dashboard")

## FLUTTER API ##
@csrf_exempt
def flutter_user_login(request):
    if request.method == "POST":
        data = request.body.decode("utf-8")
        cleaned_data = json.loads(data)
        telephone = cleaned_data["telephone"]
        password = cleaned_data["password"]
        
        user = authenticate(request, telephone=telephone, password=password)
        
        if user:
            login(request, user)
            try:
                admin = user.admin
                return JsonResponse({"session-id": request.session.session_key, "is_admin": True, "is_citizen": False, "type":admin.type, "telephone":admin.owning_user.telephone})
            
            except Admin.DoesNotExist:
                return JsonResponse({"session-id": request.session.session_key, "is_admin": False, "is_citizen": True, "telephone": user.telephone, "is_verified": user.accountmasyarakat.is_verified})
        else:
            return HttpResponseForbidden()


def flutter_test(request):
    id = request.GET.get("sessionid")
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(id)

@csrf_exempt
def flutter_user_logout(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode("utf-8"))
        session_id = data.get('session_id')

        engine = import_module(settings.SESSION_ENGINE)
        sessionstore = engine.SessionStore
        session = sessionstore(session_id)

        session.flush()

        return JsonResponse({'isLoggedOut':True}, safe=False)

def flutter_get_user_info(request):
    if request.method == 'GET':
        data = request.GET
        session_id = data.get('session_id')
        engine = import_module(settings.SESSION_ENGINE)
        sessionstore = engine.SessionStore
        session = sessionstore(session_id)
        user_telephone = session.get('_auth_user_id')
        corresponding_account = Account.objects.get(telephone=user_telephone)
        akun = AccountMasyarakat.objects.get(owning_user = corresponding_account )
        tes = ''
        if akun.is_verified:
            tes = 'VERIFIED'
        else:
            tes = 'UNVERIFIED'
        
        data_dictionary = {
            'name': akun.name,
            'NIK':akun.NIK,
            'SKTM' : akun.SKTMLink,
            'BukuTabungan': akun.bukuTabunganLink,
            'Area': akun.partOf.area_name,
            'verification' : tes
            

        }

        return JsonResponse(data_dictionary, safe=False)
@csrf_exempt
def flutter_update_user(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body.decode("utf-8"))
            session_id = data.get('session_id')
            engine = import_module(settings.SESSION_ENGINE)
            sessionstore = engine.SessionStore
            session = sessionstore(session_id)
            user_telephone = session.get('_auth_user_id')
            corresponding_account = Account.objects.get(telephone = user_telephone)
            akun = AccountMasyarakat.objects.get(owning_user = corresponding_account)
            akun.SKTMLink = data.get('sktm')
            akun.bukuTabunganLink = data.get('buku')
            akun.is_verified = False
            akun.save()
            return JsonResponse({'isSuccessful':True},safe = False)
        except Exception as E:
            return JsonResponse({'isSuccesful':False,'error': str(E)},safe = True)
            
@csrf_exempt
def get_user_bansos(request):
    # Get all bansos group
    data = json.loads(request.body.decode('utf-8'))
    session_id = data.get('sessionId')
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(session_id)
    user_telephone = session.get('_auth_user_id')
    user = AccountMasyarakat.objects.get(owning_user=user_telephone)
    bansosgroup = Bansos.objects.filter(owning_group__status = 'received', received_by = user)
    all_bansos_list = []
    for grup in bansosgroup:
        group = grup.owning_group
        previous_locations = BansosPreviousLocation.objects.filter(bansos_group = group,status = 'received')
        for prev in previous_locations:
            s = prev.time_stamp
        group_dict = {
            "pk":str(group.id_bansos_group),
            "fields": {
            "responsible_admin": group.responsible_admin.owning_user.telephone,
            "receiving_provinsi":group.receiving_provinsi.area_name,
            "status":group.status,
            "destination_kelurahan":group.destination_kelurahan,
            "destination_kecamatan":group.destination_kecamatan,
            "destination_kabupaten_kota":group.destination_kabupaten_kota,
            "total_receiver":group.total_receiver,
            "bentuk_bantuan": group.bentuk_bantuan,
            "satuan_bantuan": group.satuan_bantuan,
            "timestamp" : str(s)
            }
        }
        all_bansos_list.append(group_dict)
    

    # Serialize query set to json
    data = json.dumps(all_bansos_list)

    return HttpResponse(data, content_type='application/json')
            
@csrf_exempt
def get_user_bansos_info(request):
    # Get all bansos group
    data = request.GET
    session_id = data.get('session_id')
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(session_id)
    user_telephone = session.get('_auth_user_id')
    user = AccountMasyarakat.objects.get(owning_user=user_telephone)
    bansosgroup = Bansos.objects.filter(received_by = user)
    all_bansos_list = []
    for grup in bansosgroup:
        group = grup.owning_group
        previous_locations = BansosPreviousLocation.objects.filter(bansos_group = group)
        tes = previous_locations[0]
        if group.status == 'received':
            continue
        else:
            for prev in previous_locations:
                s = prev.time_stamp
            group_dict = {
                "pk":str(group.id_bansos_group),
                "fields": {
                "responsible_admin": group.responsible_admin.owning_user.telephone,
                "receiving_provinsi":group.receiving_provinsi.area_name,
                "status":group.status,
                "destination_kelurahan":group.destination_kelurahan,
                "destination_kecamatan":group.destination_kecamatan,
                "destination_kabupaten_kota":group.destination_kabupaten_kota,
                "total_receiver":group.total_receiver,
                "bentuk_bantuan": group.bentuk_bantuan,
                "satuan_bantuan": group.satuan_bantuan,
                "timestamp" : str(s)
                }
            }
            all_bansos_list.append(group_dict)
        
        

    # Serialize query set to json
    data = json.dumps(all_bansos_list)

    return HttpResponse(data, content_type='application/json')

def user_bansos_from_id(request):
    bansos_id = request.GET.get("bansos-id")
    bansos = BansosGroup.objects.filter(id_bansos_group=bansos_id)
    
    if bansos:

        bansos = bansos[0]
        previous_locations = BansosPreviousLocation.objects.filter(bansos_group=bansos)
        prevs = previous_locations.last()
        corresponding_data = {
            "pk":str(bansos.id_bansos_group),
            "admin": {
                "name":bansos.responsible_admin.name,
                "telephone":bansos.responsible_admin.owning_user.telephone
            },
            "area":{
                "area_code": bansos.receiving_provinsi.area_code,
                "des_urban_village":bansos.destination_kelurahan,
                "des_subdistrict":bansos.destination_kecamatan,
                "des_city":bansos.destination_kabupaten_kota,
                "des_provinsi":bansos.receiving_provinsi.area_name,
                "status": bansos.status,
                "kelurahan": bansos.kelurahan,
                "kecamatan" : bansos.kecamatan,
                "kabupaten_kota" : bansos.kabupaten_kota,
                "provinsi" : bansos.provinsi,
                "responsible_admin": bansos.responsible_admin.name,
                "kelurahan2" : prevs.kelurahan,
                "kecamatan2" : prevs.kecamatan,
                "provinsi2" : prevs.provinsi,
                "status2" : prevs.provinsi,
                "time2" : str(prevs.time_stamp)
            },
            "bentuk_bantuan": bansos.bentuk_bantuan,
            "satuan_bantuan": bansos.satuan_bantuan,
            "total_receiver": bansos.total_receiver,
        }



        # location_dictionaries = []
        # for prev in previous_locations:
        #     tempDict = {
        #         "kelurahan":prev.kelurahan,
        #         "kecamatan":prev.kecamatan,
        #         "kabupaten_kota":prev.kabupaten_kota,
        #         "provinsi":prev.provinsi,
        #         "status":prev.status,
        #         "time_stamp":str(prev.time_stamp),
        #     }

        #     location_dictionaries.append(tempDict)
        
        # corresponding_data["previous_locations"] = location_dictionaries

        # Get total package informations


        # Get corresponding previous locations

        data = json.dumps(corresponding_data)

        return HttpResponse(data, content_type='application/json')
    else:
        return HttpResponse("Object not found")





