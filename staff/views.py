from importlib import import_module
from django.conf import settings
from django.shortcuts import redirect, render
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from user.models import AccountMasyarakat
from .models import Admin
from django.http.response import HttpResponse, HttpResponseRedirect, HttpResponseForbidden, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from user.models import Account
import json


@login_required()
def admin_info(request):
    adInfo = request.user
    try:
        admin = adInfo.admin
    
    except Admin.DoesNotExist:
        return HttpResponseForbidden()

    response = {'Admin_Info' : adInfo}
    return render(request, 'bansospeduli_admininfo.html', response)

@login_required()
def verification(request):

    adInfo = request.user
    try:
        admin = adInfo.admin
    
    except Admin.DoesNotExist:
        return HttpResponseForbidden()

    if adInfo.admin.type != 'verification':
        return HttpResponseForbidden()

    response = {'Admin_Info' : adInfo}

    if adInfo.admin.type=='verification':
        assign_user = adInfo.admin.accountmasyarakat_set.all()
        response['assigned'] = assign_user

    return render(request, 'bansospeduli_Dashboard.html', response)

@login_required()
def user_verify(request, user_telephone):
    user = request.user

    try:
        admin_object = user.admin

    except Admin.DoesNotExist:
        return HttpResponseForbidden()

    if admin_object.type != 'verification':
        return HttpResponseForbidden()

    # Get Account to verify
    user_to_verify = Account.objects.get(telephone = user_telephone)

    # Try retrieving account masyarakat 
    try:
        account_masyarakat = user_to_verify.accountmasyarakat
    
    except:
        return HttpResponse("The user you are trying to edit is not a Citizen.")


    if account_masyarakat.verifying_admin != admin_object:
        return render(request, '403.html')

    response = {'user':user, 'user_to_verify': user_to_verify}
    return render(request, 'bansospeduli_verification.html', response)

@login_required()
def reject(request, user_telephone):
    user = request.user

    try:
        admin_object = user.admin

    except Admin.DoesNotExist:
        return HttpResponseForbidden()

    if admin_object.type != 'verification':
        return HttpResponseForbidden()

    # Get Account to verify
    user_to_verify = Account.objects.get(telephone = user_telephone)

    # Try retrieving account masyarakat
    try:
        account_masyarakat = user_to_verify.accountmasyarakat

    except:
        return HttpResponse("The user you are trying to edit is not a Citizen.")

    if account_masyarakat.verifying_admin != admin_object:
        return HttpResponseForbidden()

    user_to_verify.accountmasyarakat.is_verified = False
    user_to_verify.accountmasyarakat.save()

    return redirect('verification')

@login_required()
def accept(request, user_telephone):
    user = request.user

    try:
        admin_object = user.admin

    except Admin.DoesNotExist:
        return HttpResponseForbidden()

    if admin_object.type != 'verification':
        return HttpResponseForbidden()
        
    # Get Account to verify
    user_to_verify = Account.objects.get(telephone = user_telephone)

    # Try retrieving account masyarakat
    try:
        account_masyarakat = user_to_verify.accountmasyarakat

    except:
        return HttpResponse("The user you are trying to edit is not a Citizen.")
    
    if account_masyarakat.verifying_admin != admin_object:
        return HttpResponseForbidden()

    user_to_verify.accountmasyarakat.is_verified = True
    user_to_verify.accountmasyarakat.save() 
    
    return redirect('verification')

def getMasyarakat(request, admin_tele):
    admin_account_object = Account.objects.get(telephone=admin_tele)
    data = [(i.owning_user.telephone, i.is_verified, i.name) for i in AccountMasyarakat.objects.filter(verifying_admin=admin_account_object.admin)]
    dataSer = json.dumps(data)
    return HttpResponse(dataSer, content_type="application/json")

def mobile_get_admin_info(request):
    '''
    Method to return name, admin_type, and registered phone number
    '''
    # request dictionary
    
    if request.method == 'GET':
        data = request.GET
        session_id = data.get('session_id')

        engine = import_module(settings.SESSION_ENGINE)
        sessionstore = engine.SessionStore
        session = sessionstore(session_id)

        admin_telephone = session.get('_auth_user_id')


        # Get corresponding account from telephone
        corresponding_account = Account.objects.get(telephone=admin_telephone)

        # Get corresponding admin object from account
        corresponding_admin = corresponding_account.admin

        # Construct dictionary to be returned as JSON
        data_dictionary = {
            'name':corresponding_admin.name,
            'admin_type':corresponding_admin.type,
            'phone_number':corresponding_account.telephone
        }

        # json_data = json.dumps(data_dictionary)

        return JsonResponse(data_dictionary, safe=False)

def mobile_get_verification_responsibility(request):
    '''
    Method to return the list of users to verify from 
    a verification admin session (phone number)
    '''
    if request.method == 'GET':
        data = request.GET
        session_id = data.get('session_id')

        engine = import_module(settings.SESSION_ENGINE)
        sessionstore = engine.SessionStore
        session = sessionstore(session_id)

        # Get the telephone from data dictionary
        admin_telephone = session.get('_auth_user_id')

        # Get corresponding account from telephone
        corresponding_account = Account.objects.get(telephone=admin_telephone)

        # Get corresponding admin from account
        corresponding_admin = corresponding_account.admin

        # Get accounts who were assinged to the corresponding admin
        resp_accounts = AccountMasyarakat.objects.filter(verifying_admin=corresponding_admin)

        json_data = serializers.serialize('json', resp_accounts)

        return HttpResponse(json_data, content_type='application/json')

@csrf_exempt
def mobile_verify_user(request):

    if request.method == 'POST':
        try:
            data = json.loads(request.body.decode("utf-8"))

            # Assign data to corresponding variables
            citizen_telephone = data.get('telephone')
            verification_result = data.get('verification_result')

            # Get corresponding account from telephone
            corresponding_account = Account.objects.get(telephone=citizen_telephone)

            # Get corresponding masyarakat from account
            corresponding_masyarakat = corresponding_account.accountmasyarakat

            if verification_result == 'verified':
                corresponding_masyarakat.is_verified=True
            else:
                corresponding_masyarakat.is_verified=False
            
            corresponding_masyarakat.save()
            
            return JsonResponse({'is_successful':True, 'error':None})
            
        except Exception as exc:
            return JsonResponse({'is_successful':False, 'error':str(exc)})

def mobile_get_user_from_tele(request):
    
    if request.method == 'GET':
        
        data = request.GET

        # Assign data to corresponding variables
        citizen_telephone = data.get('telephone')

        # Get corresponding account from telephone
        corresponding_account = Account.objects.get(telephone=citizen_telephone)

        # Get corresponding masyarakat from account
        corresponding_masyarakat = corresponding_account.accountmasyarakat

        data_dictionary = {
            'phone_number':corresponding_account.telephone,
            'name':corresponding_masyarakat.name,
            'nik': corresponding_masyarakat.NIK,
            'sktm': corresponding_masyarakat.SKTMLink,
            'bukutabungan':corresponding_masyarakat.bukuTabunganLink,
            'location':corresponding_masyarakat.partOf.area_name,
            'isVerified':corresponding_masyarakat.is_verified
        }
        return JsonResponse(data_dictionary, safe=False)








