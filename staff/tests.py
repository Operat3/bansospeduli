from django.contrib.auth import login
from django.http.request import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from staff.views import accept
from user.models import Account, AccountMasyarakat
from staff.models import Admin
from distribusi.models import Area, BansosGroup

class StaffTestCase(TestCase):
    # Check Login Required
    def test_admin_info_is_login_required(self):
        response = Client().get('/staff/admin-info')
        self.assertEqual(response.status_code, 302)

    def test_verification_is_login_required(self):
        response = Client().get('/staff/verification')
        self.assertEqual(response.status_code, 302)

    # Test User Verify
    def test_user_verify(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        account_masyarakat = Account(telephone='082312330001')
        account_masyarakat.set_password('helloworld')
        masyarakat_object = AccountMasyarakat(owning_user=account_masyarakat, is_verified=False, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
        account_masyarakat.save()
        masyarakat_object.save()

        response = self.client.get('/staff/user-verify/' + account_masyarakat.telephone)

        self.assertEqual(response.status_code, 302)

    def test_admin_info(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/staff/admin-info')
        self.assertEqual(response.status_code, 200)
        
    # Check 200
    def test_admin_info(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/staff/admin-info')
        self.assertEqual(response.status_code, 200)
    
    def test_verification(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='verification', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')
        
        response = self.client.get('/staff/verification')
        self.assertEqual(response.status_code, 200)
    
    # Test User Verify
    def test_user_verify(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        self.account = Account(telephone='082312330002')
        self.account.set_password('helloworld')
        verifying_admin = Admin(owning_user=self.account, type='verification', name='Winaldo')
        self.account.save()
        verifying_admin.save()

        account_masyarakat = Account(telephone='082312330001')
        account_masyarakat.set_password('helloworld')
        masyarakat_object = AccountMasyarakat(owning_user=account_masyarakat, is_verified=False, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
        account_masyarakat.save()
        masyarakat_object.save()

        self.client.login(telephone='082312330002', password='helloworld')

        response = self.client.get('/staff/user-verify/' + account_masyarakat.telephone)
        
        self.assertEqual(response.status_code, 200)

    def test_accept_user(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        self.account = Account(telephone='0823123300023')
        self.account.set_password('12345678')
        verifying_admin = Admin(owning_user=self.account, type='verification', name='Winaldo')
        self.account.save()
        verifying_admin.save()

        account_masyarakat = Account(telephone='082312330001')
        account_masyarakat.set_password('helloworld')
        masyarakat_object = AccountMasyarakat(owning_user=account_masyarakat, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
        account_masyarakat.save()
        masyarakat_object.save()

        self.client.login(telephone='0823123300023', password='12345678')

        response = self.client.get('/staff/accept/' + account_masyarakat.telephone)
        
        self.assertEqual(response.status_code, 302)
    
    def test_reject_user(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        self.account = Account(telephone='0823123300023')
        self.account.set_password('12345678')
        verifying_admin = Admin(owning_user=self.account, type='verification', name='Winaldo')
        self.account.save()
        verifying_admin.save()

        account_masyarakat = Account(telephone='082312330001')
        account_masyarakat.set_password('helloworld')
        masyarakat_object = AccountMasyarakat(owning_user=account_masyarakat, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
        account_masyarakat.save()
        masyarakat_object.save()

        self.client.login(telephone='0823123300023', password='12345678')

        response = self.client.get('/staff/reject/' + account_masyarakat.telephone)
        
        self.assertEqual(response.status_code, 302)
    
    # 403 Testing
    def test_403_admin_info(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        self.account = Account(telephone='082312330002')
        self.account.set_password('helloworld')
        verifying_admin = Admin(owning_user=self.account, type='verification', name='Winaldo')
        self.account.save()
        verifying_admin.save()

        account_masyarakat = Account(telephone='082312330001')
        account_masyarakat.set_password('helloworld')
        masyarakat_object = AccountMasyarakat(owning_user=account_masyarakat, is_verified=False, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
        account_masyarakat.save()
        masyarakat_object.save()

        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/staff/admin-info')
        
        self.assertEqual(response.status_code, 403)
    
    def test_403_verification_citizen(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()

        self.account = Account(telephone='082312330002')
        self.account.set_password('helloworld')
        verifying_admin = Admin(owning_user=self.account, type='verification', name='Winaldo')
        self.account.save()
        verifying_admin.save()

        account_masyarakat = Account(telephone='082312330001')
        account_masyarakat.set_password('helloworld')
        masyarakat_object = AccountMasyarakat(owning_user=account_masyarakat, is_verified=False, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
        account_masyarakat.save()
        masyarakat_object.save()

        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/staff/verification')
        
        self.assertEqual(response.status_code, 403)

    def test_403_verification_other_admin(self):
        self.client = Client()

        self.account = Account(telephone='082312330002')
        self.account.set_password('helloworld')
        verifying_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        verifying_admin.save()

        self.client.login(telephone='082312330002', password='helloworld')

        response = self.client.get('/staff/verification')
        
        self.assertEqual(response.status_code, 403)
    
    def test_admin(self):
        self.account = Account(telephone='082312330002')
        self.account.set_password('helloworld')
        verifying_admin = Admin(owning_user=self.account, type='verifying', name='Winaldo')
        self.account.save()
        verifying_admin.save()

        self.assertEqual('082312330002', self.account.telephone)
        self.assertEqual(self.account.admin.type, 'verifying')
        self.assertEqual(self.account.admin.name, 'Winaldo')

    # def test_accept_user_with_function(self):
    #     new_area = Area(area_code='JKT', area_name='JAKARTA')
    #     new_area.save()

    #     self.client = Client()

    #     self.account = Account(telephone='0823123300023')
    #     self.account.set_password('12345678')
    #     verifying_admin = Admin(owning_user=self.account, type='verification', name='Winaldo')
    #     self.account.save()
    #     verifying_admin.save()

    #     account_masyarakat = Account(telephone='082312330001')
    #     account_masyarakat.set_password('helloworld')
    #     masyarakat_object = AccountMasyarakat(owning_user=account_masyarakat, name='Winaldo', NIK='32312312', SKTMLink='google.com', bukuTabunganLink='asd.com', partOf=new_area, verifying_admin=verifying_admin)
    #     account_masyarakat.save()
    #     masyarakat_object.save()

    #     self.client.login(telephone='0823123300023', password='12345678')

    #     request = HttpRequest()
        
    #     request.user = self.account
    #     response = accept(request, account_masyarakat.telephone)
        
    #     self.assertEqual(account_masyarakat.accountmasyarakat.is_verified, True)




    


    