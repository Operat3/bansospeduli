from django.contrib import admin
from django.urls import include, path

from .views import accept, admin_info, getMasyarakat, mobile_get_admin_info, mobile_get_user_from_tele, mobile_get_verification_responsibility, mobile_verify_user, user_verify, verification, reject

urlpatterns = [
    path('admin-info', admin_info, name = 'add_info'),
    path('verification', verification, name = 'verification'),
    path('reject/<str:user_telephone>', reject, name = 'rejected'),
    path('user-verify/<str:user_telephone>', user_verify, name = 'verified'),
    path('accept/<str:user_telephone>', accept, name = 'accepted'),
    path('get-masyarakat/<str:admin_tele>', getMasyarakat, name = 'get-masyarakat'),
    path('flutter-get-admin-info/', mobile_get_admin_info),
    path('flutter-get-tracking-responsibilities/', mobile_get_verification_responsibility),
    path('flutter-verification-citizen/', mobile_verify_user),
    path('flutter-get-user-from-tele/', mobile_get_user_from_tele)
]