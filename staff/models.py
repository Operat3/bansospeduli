from django.db import models
from django.db.models.fields import CharField


# Create your models here.
# class Admin(models.Model):
#     owning_user = models.OneToOneField(User, on_delete=models.CASCADE)
#     type = models.CharField(max_length=20)

class Admin(models.Model):
    owning_user = models.OneToOneField("user.Account", on_delete=models.CASCADE)
    type = models.CharField(max_length=20)
    name = models.CharField(max_length=20, null=True)

    # This attribute is dedicated only for tracking admin.
    # It indicates whether a tracking admin is currently 'guarding'
    # a bansos group or not.
    is_occupied = models.BooleanField(default=False)
