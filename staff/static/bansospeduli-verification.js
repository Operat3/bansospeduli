const admin_tele_inp = document.getElementById("admin_tele");
var admin_tele;

$(document).ready(verifyStatus);
function verifyStatus() {
    admin_tele = admin_tele_inp.value;
    $.ajax({
        url:'/staff/get-masyarakat/' + admin_tele, //Specifies the URL to send the request to
        type: 'GET', 	//Specifies the type of request.
         	
        success: function(data) {
            $('#verified_user_container').html('');
            $('#unverified_user_container').html('');
            var breakk = document.createElement('br');
            for(let i = 0; i < data.length; i++) {
                var accountMasyarakat = data[i];
                var div_a = document.createElement('div');
                div_a.setAttribute('class', 'masyarakat-div')

                var a_masyarakat = document.createElement('a');
                a_masyarakat.setAttribute('href', "../staff/user-verify/"+accountMasyarakat[0]);
                a_masyarakat.innerText = accountMasyarakat[0] + " - " + accountMasyarakat[2];
                
                div_a.append(a_masyarakat);

                if (accountMasyarakat[1]){
                    $('#verified_user_container').append(div_a);
                   
                } else {
                    $('#unverified_user_container').append(div_a);

                }
            }
            setTimeout(verifyStatus, 5000);
        }, //A function to be run when the request succeeds

      });
      
}