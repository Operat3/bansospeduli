# Group I04
![pipeline status](https://gitlab.com/pbp-project-i04/bansospeduli/badges/master/pipeline.svg)
![coverage report](https://gitlab.com/pbp-project-i04/bansospeduli/badges/master/coverage.svg)
## Members:
1. Anne Yuliana
<<<<<<< HEAD
2. Rashad Aziz
3. Abdul Mughni Wibisono - 2006607476
=======
2. Rashad Aziz - 2006519984
3. Abdul Mughni Wibisono
>>>>>>> f65082810d23369e5b01996b1e7527c01a2bb877
4. David Julius Vos
5. Raihan Fadhila Sukmana - 2006519971
6. Johanes Christian Lewi Putrael T - 2006519946
7. Winaldo Amadea Hestu

## Link To Heroku Application 
https://bansos-peduli.herokuapp.com/

## PORTAL BANTUAN SOSIAL KHUSUS COVID
### Project Description and Usefullness
This app will help in digitalizing the distribution of bansos. With this app, users can apply to be a receipient of bansos whose data will be first verified by an admin team. Users are able to track the progress of their bansos, as well as to know the details of the bansos that belongs to them. If the contents of the bansos received by the user is not in accordance with the details available in the website, users are able to file a complain that will reviewed by customer service. By implementing such a system, bansos distribution would be more transparent which cancels out the possibility of corruption within the process of distribution along with inadequate standards of bansos received. This would provide a medium for bansos receipients to anonymously (staffs on the field) report misbehaviour along with quality issues. In addition the homepage will also provide Covid-19 Data coupled with the flow of registration.

## Modules (Applications)

### /BERANDA (Covid Data and Registration and Distribution Flow)

### /REGISTRATION (For ADMIN and USER)

### /USER 
    Endpoints: 
    - /DASHBOARD
    - /HISTORY

### /DASHBOARD-LAPORAN
    Endpoints:
    - /AJUKAN (For User)
    - /KELUHAN (For Admin)
        - /USER/ID-LAPORAN

### /STAFF
    Endpoints:
    - /DASHBOARD (Consists of Personal Information, Job Description)
    - /VERIFY-USER (LIST USER REGISTRATIONS)

### /DISTRIBUSI
    Endpoints:
    - /GENERATE-BANSOS (SEMUA ATTRIB BANSOS)
    - /SHIPPING-STATUS 


## User Persona
### Non-logged in Visitor
This user can only access the homepage where he/she can view the Covid-19 related information and registration steps for the application and bansos recepients. This user can also register themselves to be a bansos recepient candidate by filling out the registration form. 

### Logged-in (Unverified) Citizen
These users are those who have registered and are waiting for verification from the verification staff. The followings are taken into consideration in the process of verification: consistency between NIK, NO KK, and Phone Number, and a the validity of SKTM. While being unverified, the user can view his/her verification status and personal data that is retrieved from the server database. The user, however, cannot change the data displayed. 

### Logged-in (Verified) Citizen
These users are those who have registered and now are eligible to receive bansos. They can view their personal data, their current assigned bansos data (that includes identification number of bansos, the current status (in queue, on the way, received), type of bansos (type and amount), and current position), their bansos history. In addition, they will have access to a page that consist a form to report any malpractice or file any complaints. 

### Verification Admin
A verification admin is able to view a list of unverified users who have just registered. A verification admin will be the one who decides whether or not the unverified user is eligible for bansos. In the verification process they will ensure the authenticity of the SKTM (manually).

### Generator Admin
This type of staff is responsible in generating the bansos that will be distributed to a certain area. He/she determines the type and amount of the bansos that is going to be distributed. The generator admin will distribute the bansos in groups. He/she can assign each generated bansos to a chosen tracking admin from a list.

### Tracking Admin
This type of staff is responsible for updating the position and status of the bansos. Ideally the tracking admin will physically follow the bansos distribution process. However this can be susbstituted with having people on the field communicating.

### Customer Service
Customer Service staff are able to view a list of reports filed by bansos receivers. They will manually review complaints and can either accept or reject the complaint. If the complaint is accepeted an ivestigation will be conducted and the complaint will be forwarded to the super admin.

### Super Admin
He/she is the highest level of authority in the app. Ideally, he/she is a member of the ministry of social affairs that can view filtered reports (by cutomer service), and can also view the entire admin database that includes the queued, sent and received bansos groups. He/she can also revoke administrative powers. He/she is also the one assigns administrative roles to each staff.

## THINGS TO DO:
* MODELS
* REGISTRATION APP (MASYARAKAT, STAFF)
* FORUM LAPORAN
* DASHBOARD USER (DATA DIRI, DATA BANSOS, HISTORY, LAPORAN)
* DASHBOARD ADMIN (DATA DIRI)
    - SUPER
    - VERIFIKASI (BISA ACC MASYARAKAT)
    - GENERATE (BISA MEMBUAT/MENGIRIM BANSOS)
    - TRACKING RESPONSIBLE_BANSOS (UPDATE STATUS BANSOS)
    - CUSTOMER SERVICE (BISA NGELIAT KELUHAN)

## Job Division
* Anne --> STAFF
* Mughni --> HOME
* Johanes --> DISTRIBUSI
* Raihan --> REGISTRASI
* Rashad --> DASHBOARD-LAPORAN
* Winaldo --> USER
